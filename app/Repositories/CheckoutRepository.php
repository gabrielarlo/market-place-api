<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Repositories\CartRepository;
use App\Interfaces\CheckoutInterface;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Validator;

class CheckoutRepository implements CheckoutInterface
{
    public function list($req)
    {
        $user_id = auth()->user()->id;
        $list = Cart::where('user_id', $user_id)->where('is_deleted', 0)->whereHas('items', function ($q) {
            $q->where('is_deleted', 0)->where('is_ordered', 0)->having('on_checkout', '>', 0);
        })->get();
        $list->makeHidden(['is_deleted', 'created_at', 'updated_at']);

        $total = (new CartRepository)->calculateTotalAmount();
        $shipping = 0;

        return res('Success', compact('list', 'total', 'shipping'));
    }

    public function placeOrder($req)
    {
        return (new OrderRepository())->place($req);
    }
}
