<?php

namespace App\Repositories;

use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Interfaces\StoreInterface;

class StoreRepository implements StoreInterface
{
    public function find($req)
    {
        $limit = 10;
        if ($req->has('limit')) {
            $limit = $req->limit;
        }

        $city = 'All';
        $category = 'All';
        if ($req->has('city')) {
            $city = $req->city;
        }
        if ($req->has('category')) {
            $category = $req->category;
        }

        if (strtolower($category) == 'all') {
            $category_ids = Category::pluck('id');
        } else {
            $category_ids = Category::where('name', $category)->pluck('id');
        }
        $product_ids = ProductCategory::whereIn('category_id', $category_ids)->pluck('product_id')->unique();
        $store_ids = Product::whereIn('id', $product_ids)->pluck('store_id')->unique();

        if (strtolower($city) == 'all') {
            $stores = Store::whereIn('id', $store_ids)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit)->get()->shuffle();
        } else {
            $stores = Store::whereIn('id', $store_ids)->where('city', $city)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit)->get()->shuffle();
        }
        $stores->makeHidden(['qid', 'eid', 'commercial_registration', 'commercial_permit', 'updated_at', 'is_approved', 'ban', 'approved_at']);

        $found_store_ids = $stores->pluck('id');
        $other_stores = Store::whereNotIn('id', $found_store_ids)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit - $stores->count())->get()->shuffle();
        $other_stores->makeHidden(['qid', 'eid', 'commercial_registration', 'commercial_permit', 'updated_at', 'is_approved', 'ban', 'approved_at']);

        return res('Success', [
            'found' => $stores,
            'alternative' => $other_stores
        ]);
    }

    public function details($req)
    {
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        if ($req->store_id == 0 && $req->has('user_id')) {
            $store = Store::where('user_id', $req->user_id)->where('ban', 0)->where('sell_on_country', session('iso'))->first();
        } else {
            $store = Store::where('id', $req->store_id)->where('ban', 0)->where('sell_on_country', session('iso'))->first();
        }
        if (!$store) {
            return res(__('validation.not_found'), null, 400);
        }

        return res('Success', $store);
    }
}
