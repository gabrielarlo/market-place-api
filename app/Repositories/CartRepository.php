<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Store;
use App\Models\Product;
use App\Models\CartItem;
use App\Interfaces\CartInterface;
use Illuminate\Support\Facades\Validator;

class CartRepository implements CartInterface
{
    public function list($req)
    {
        $user_id = auth()->user()->id;
        $list = Cart::where('user_id', $user_id)->where('is_deleted', 0)->get();
        $list->makeHidden(['is_deleted', 'created_at', 'updated_at']);
        $list = collect($list);
        $list = $list->filter(function ($item) {
            return count($item->items_info) > 0;
        });
        $select_all = false;

        $cart_count = Cart::where('user_id', $user_id)->where('is_deleted', 0)->count();
        $on_checkout_count = Cart::where('user_id', $user_id)->where('is_deleted', 0)->where('on_checkout', 1)->count();
        if ($on_checkout_count >= $cart_count) {
            $select_all = true;
        }
        $total = $this->calculateTotalAmount();

        return res('Success', compact('list', 'select_all', 'total'));
    }

    public function calculateTotalAmount() {
        $total = 0;
        $user_id = auth()->user()->id;
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $items = CartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('on_checkout', 1)->where('is_ordered', 0)->get();
        foreach ($items as $item) {
            if ($item->product->getIsOnSaleAttribute() === 1) {
                $total += $item->product->sale_price * $item->quantity;
            } else {
                $total += $item->product->price * $item->quantity;
            }
        }
        return $total;
    }

    public function add($req)
    {
        $validator = Validator::make($req->all(), [
            'product_id' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $product = Product::where('id', $req->product_id)->where('is_deleted', 0)->where('status', 1)->first();
        if (!$product) {
            return res(__('validation.not_found'), null, 400);
        }
        $store = Store::where('id', $product->store_id)->where('is_approved', 1)->where('ban', 0)->first();
        if (!$store) {
            return res(__('validation.not_found'), null, 400);
        }

        $cart = $this->fillCart($store);
        $this->addCartItem($cart, $product);

        return res('Product Added to Cart');
    }

    private function fillCart(Store $store)
    {
        $user_id = auth()->user()->id;
        $cart = Cart::where('user_id', $user_id)->where('store_id', $store->id)->where('is_deleted', 0)->first();
        if (!$cart) {
            $cart = new Cart;
            $cart->user_id = $user_id;
            $cart->store_id = $store->id;
        }
        $cart->save();
        return $cart;
    }

    private function addCartItem(Cart $cart, Product $product)
    {
        $item = CartItem::where('cart_id', $cart->id)->where('product_id', $product->id)->where('is_deleted', 0)->where('is_ordered', 0)->first();
        if (!$item) {
            $item = new CartItem;
            $item->cart_id = $cart->id;
            $item->product_id = $product->id;
            $item->quantity = 1;
        } else {
            $item->quantity = $item->quantity + 1;
        }
        $item->save();
        return $item;
    }

    public function count($req)
    {
        $user_id = auth()->user()->id;
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $item_count = CartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->sum('quantity');
        $item_count = (int)$item_count;

        return res('Success', $item_count);
    }

    public function clear($req)
    {
        $user_id = auth()->user()->id;
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');

        Cart::whereIn('id', $cart_ids)->update(['is_deleted' => 1]);
        CartItem::whereIn('cart_id', $cart_ids)->update(['is_deleted' => 1]);

        return res('Cart Cleared');
    }

    public function updateQuantity($req)
    {
        if (!$req->has('cart_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('quantity')) {
            return res('Quantity is required', null, 400);
        }

        $item = CartItem::where('cart_id', $req->cart_id)->where('product_id', $req->product_id)->where('is_deleted', 0)->where('is_ordered', 0)->first();
        if (!$item) {
            return res(__('validation.not_found'), null, 400);
        }
        $product = Product::find($req->product_id);
        if (!$product) {
            return req(__('validation.not_found'), null, 400);
        }

        $qty = $req->quantity;
        $total_item = $product->getInventoryInfoAttribute();
        $total = $this->calculateTotalAmount();
        if ($qty <= 0) {
            $qty = 1;
            return res(__('validation.invalid_quantity'), [
                'qty' => (int)$qty,
                'total' => $total
            ], 200);
        }
        if ($qty > $total_item) {
            $qty = $total_item;
            return res(__('validation.invalid_quantity'), [
                'qty' => (int)$qty,
                'total' => $total
            ], 200);
        }

        $item->quantity = $qty;
        $item->save();

        $total = $this->calculateTotalAmount();

        return res('Success', [
            'qty' => (int)$qty,
            'total' => $total
        ]);
    }

    public function toggleCheckout($req)
    {
        if (!$req->has('cart_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('switch')) {
            return res('Switch is required', null, 400);
        }
        $user_id = auth()->user()->id;

        if ($req->cart_id == 0) {
            Cart::where('user_id', $user_id)->where('is_deleted', 0)->update(['on_checkout' => $req->switch]);
            $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
            CartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
        } else {
            $cart = Cart::where('id', $req->cart_id)->where('is_deleted', 0)->first();
            if (!$cart) {
                return res(__('validation.not_found'), null, 400);
            }
            if ($req->product_id == 0) {
                CartItem::where('cart_id', $req->cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
                Cart::where('id', $req->cart_id)->update(['on_checkout' => $req->switch]);
            } else {
                CartItem::where('cart_id', $req->cart_id)->where('product_id', $req->product_id)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
            }
        }

        $total_item_count = CartItem::where('cart_id', $req->cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->count();
        $checkout_item_count = CartItem::where('cart_id', $req->cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->where('on_checkout', 1)->count();
        if ($checkout_item_count >= $total_item_count) {
            Cart::where('id', $req->cart_id)->update(['on_checkout' => 1]);
        } else {
            Cart::where('id', $req->cart_id)->update(['on_checkout' => 0]);
        }

        return res('Success');
    }

    public function remove($req)
    {
        if (!$req->has('cart_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        CartItem::where('cart_id', $req->cart_id)->where('product_id', $req->product_id)->where('is_ordered', 0)->where('is_deleted', 0)->update(['is_deleted' => 1]);
        $item_count = CartItem::where('cart_id', $req->cart_id)->where('is_ordered', 0)->where('is_deleted', 0)->count();
        if ($item_count === 0) {
            Cart::where('id', $req->cart_id)->update(['is_deleted' => 1]);
        }

        return res('Success');
    }
}
