<?php

namespace App\Repositories;

use App\Models\State;
use App\Models\Country;
use App\Interfaces\CountryInterface;

class CountryRepository implements CountryInterface
{
    public function default($req)
    {
        $iso = iso2ToIso3(geoip()->getLocation(geoip()->getClientIP())->iso_code);
        $in_list = Country::where('iso', $iso)->count();
        if ($in_list === 0) {
            $default = Country::where('default', 1)->first();
            if ($default) {
                $iso = $default->iso;
            }
        }
        $country = Country::where('iso', $iso)->where('status', 1)->first();
        if ($country) {
            return res('Success', [
                'iso' => $country->iso,
                'name' => $country->name,
                'currency' => country(iso2ToIso3($country->iso, true))->getCurrency()['iso_4217_code'],
            ]);
        }
        return res('No Default Country', null, 400);
    }

    public function list($req)
    {
        $iso = iso2ToIso3(geoip()->getLocation(geoip()->getClientIP())->iso_code);
        $in_list = Country::where('iso', $iso)->count();
        if ($in_list === 0) {
            $default = Country::where('default', 1)->first();
            if ($default) {
                $iso = $default->iso;
            }
        }
        $countries = Country::where('status', 1)->get()->map(function ($country) use ($iso) {
            $country->default = 0;
            if ($country->iso == strtolower($iso)) {
                $country->default = 1;
            }
            $country->currency = country(iso2ToIso3($country->iso, true))->getCurrency()['iso_4217_code'];
            return $country;
        })->makeHidden(['id', 'status', 'created_at', 'updated_at']);

        return res('Success', $countries);
    }

    public function states($req)
    {
        $states = State::where('iso', session('iso'))->where('state_id', 0)->where('status', 1)->orderBy('name', 'ASC')->get();
        $states->makeHidden(['created_at', 'updated_at', 'status', 'state_id', 'iso']);

        return res('Success', $states);
    }
}
