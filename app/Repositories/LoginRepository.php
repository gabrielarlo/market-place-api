<?php

namespace App\Repositories;

use App\User;
use App\Models\OauthAccessToken;
use App\Interfaces\LoginInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginRepository implements LoginInterface
{
    public function email($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->where('status', 1)->first();
        if (!$user) {
            return res(__('auth.email_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('auth.invalid_password'), null, 400);
        }

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    public function mobile($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true),
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile', $req->mobile)->where('iso', session('iso'))->where('status', 1)->first();
        if (!$user) {
            return res(__('auth.mobile_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('auth.invalid_password'), null, 400);
        }

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    public function generateToken(User $user)
    {
        $origin = session('origin');
        OauthAccessToken::where('user_id', $user->id)->where('name', $origin)->delete();
        $data = [
            'token' => $user->createToken($origin)->accessToken,
            'user' => [
                'user_id' => $user->id,
                'hashid' => encode($user->id, 'uuid'),
                'email' => $user->email,
                'mobile_prefix' => $user->mobile_prefix,
                'mobile' => $user->mobile,
                'name' => $user->name,
                'type_info' => $user->type_info,
                'gender' => $user->gender,
                'bdate' => $user->bdate,
                'status_info' => $user->status_info,
            ]
        ];
        // NOTE: Also update on API routes
        return $data;
    }

    public function logout($req)
    {
        $origin = session('origin');
        OauthAccessToken::where('user_id', auth()->id())->where('name', $origin)->delete();
        return res('Success');
    }
}
