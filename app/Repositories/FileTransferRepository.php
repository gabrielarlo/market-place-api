<?php

namespace App\Repositories;

use App\Interfaces\FileTransferInterface;
use Illuminate\Support\Facades\Validator;

class FileTransferRepository implements FileTransferInterface
{
    public function upload($req)
    {
        $validator = Validator::make($req->all(), [
            'file' => 'required|file|max:2000',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $type = $req->header('File-Type');
        if ($req->has('type')) {
            $type = $req->type;
        }
        if ($type === null || $type === '') {
            return res('Type is required', null, 400);
        }
        $entity = $req->header('File-Entity');
        if ($req->has('entity')) {
            $entity = $req->entity;
        }
        if ($entity === null || $entity === '') {
            return res('Entity is required', null, 400);
        }
        $entity_id = $req->header('File-Entity-Id');
        if ($req->has('entity_id')) {
            $entity_id = $req->entity_id;
        }
        if ($entity_id === null || $entity_id === '') {
            return res('Entity ID is required', null, 400);
        }

        $entities = [
            'vendor',
            'customer',
            'store'
        ];

        if ($type === 'document') {
            $validator = Validator::make($req->all(), [
                'file' => 'mimes:jpg,jpeg,bmp,png,gif,svg,pdf'
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }
            if (!in_array($entity, $entities)) {
                return res('Invalid entity', null, 400);
            }
            $path = $req->file('file')->storeAs('public/' . $entity . '/' . $entity_id, now()->timestamp . '-' . $req->file('file')->getClientOriginalName());

            return res('Success', $path);
        }

        return res('Undefined Type', null, 400);
    }

    public function download($req)
    {
        return res();
    }
}
