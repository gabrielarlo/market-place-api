<?php

namespace App\Repositories;

use App\User;
use App\Mail\EmailToken;
use App\Models\SecurityQuestion;
use App\Repositories\SMSRepository;
use App\Interfaces\AccountInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Repositories\LoginRepository;
use Illuminate\Support\Facades\Validator;

class AccountRepository implements AccountInterface
{
    public function refreshToken($req)
    {
        $user = User::find(auth()->user()->id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        $token = (new LoginRepository)->generateToken($user);

        return res('Refreshed', $token);
    }

    public function update($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'gender' => 'required|in:male,female',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $validator = Validator::make($req->all(), [
            'bdate' => 'required',
        ]);
        if ($validator->fails()) {
            return res(__('user.required_bdate'), null, 400);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        $user->name = $req->name;
        $user->gender = $req->gender;
        $user->bdate = $req->bdate;
        $user->save();

        return res('Success', $user);
    }

    public function changePassword($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if (!Hash::check($req->current_password, $user->password)) {
            return res(__('user.invalid_current_password'), null, 400);
        }
        $user->password = bcrypt($req->password);
        $user->save();

        return res('Success');
    }

    public function checkPassword($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('user.invalid_current_password'), null, 400);
        }
        return res('Success');
    }

    public function requestMobileChange($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true),
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if ($user->iso === session('iso') && $user->mobile === $req->mobile) {
            return res(__('user.same_mobile'), null, 400);
        }
        $other = User::where('mobile', $req->mobile)->where('id', '<>', $req->user_id)->count();
        if ($other > 0) {
            return res(__('user.existing_mobile'), null, 400);
        }
        $user->temp_mobile = $req->mobile;
        $user->mobile_token = generateMobileToken();
        $user->save();

        (new SMSRepository())->sendSecurityCode($user);

        return res(__('auth.verify_mobile', ['mobile' => getMobilePrefix(session('iso')) . $user->temp_mobile]));
    }

    public function confirmMobileChange($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile_token', $req->token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }

        $user->mobile_token = null;
        $user->mobile = $user->temp_mobile;
        $user->iso = session('iso');
        $user->temp_mobile = null;
        $user->save();

        return res('Success', [
            'mobile' => $user->mobile,
            'mobile_prefix' => getMobilePrefix($user->iso)
        ]);
    }

    public function requestEmailChange($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if ($user->email === $req->email) {
            return res(__('user.same_email'), null, 400);
        }
        $other = User::where('email', $req->email)->where('id', '<>' ,$req->user_id)->count();
        if ($other > 0) {
            return res(__('user.existing_email'), null, 400);
        }
        $user->temp_email = $req->email;
        $user->email_token = generateEmailToken();
        $user->save();

        Mail::to($user->temp_email)->bcc(config('mail.admin.address'))->send(new EmailToken($user));

        return res(__('auth.verify_email', ['email' => $user->temp_email]));
    }

    public function confirmEmailChange($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email_token', $req->token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }

        $user->email_token = null;
        $user->email = $user->temp_email;
        $user->temp_email = null;
        $user->save();

        return res('Success', [
            'email' => $user->email
        ]);
    }

    public function getSecurityQuestions($req)
    {
        $list = SecurityQuestion::where('status', 1)->pluck('question');

        return res('Success', $list);
    }

    public function checkInfo($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res('Unable to find user', null, 400);
        }
        $user->makeHidden(['store_info', 'email_verified_at', 'type', 'temp_email', 'temp_mobile']);

        return res('Success', $user);
    }
}
