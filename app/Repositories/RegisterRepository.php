<?php

namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use App\Repositories\SMSRepository;
use Illuminate\Support\Facades\Mail;
use App\Interfaces\RegisterInterface;
use App\Repositories\AddressRepository;
use Illuminate\Support\Facades\Validator;

class RegisterRepository implements RegisterInterface
{
    public function email($req, $save = true)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|min:6|alpha_num|confirmed'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $u = User::where('email', $req->email)->first();
        if ($u) {
            if ($u->verified_email == 1) {
                return res('The email has already been taken', null, 400);
            } else {
                $u->name = $req->name;
                $u->password = bcrypt($req->password);
                $u->save();
                Mail::to($u->email)->bcc(config('mail.admin.address'))->send(new EmailVerification($u));
                return res(__('auth.verify_email', ['email' => $u->email]), [
                    'name' => $u->name,
                    'email' => $u->email
                ], 200);
            }
        }

        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->iso = session('iso', getIso());
        if ($save) {
            // TODO: reserved only
        }
        $user->save();

        if ($req->has('address')) {
            $address_req = new Request();
            $address_req->replace([
                'user_id' => $user->id,
                'name' => $req->name,
                'detailed_address' => $req->address,
                'city' => $req->city,
                'state' => $req->state,
            ]);
            (new AddressRepository)->add($address_req);
        }

        return $this->sendEmailVerificationToken($req, $user->email);
    }

    public function verifyEmail($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $token = $req->token;
        $user = User::where('email_token', $token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }
        $user->verified_email = 1;
        $user->status = 1;
        $user->email_verified_at = now()->toDateTimeString();
        $user->email_token = null;
        $user->save();

        return res('Success');
    }

    public function sendEmailVerificationToken($req, $email = null)
    {
        if ($email === null) {
            $validator = Validator::make($req->all(), [
                'email' => 'required|email'
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }
            $email = $req->email;
            $user = User::where('email', $email)->first();
            if (!$user) {
                return res(__('auth.email_not_found'), null, 400);
            }
        }

        $user = User::where('email', $email)->first();
        if (!$user) {
            $user = User::where('temp_email', $email)->first();
            if (!$user) {
                return res(__('user.not_found'), null, 400);
            }
        }
        if ($user->verified_email === 1) {
            return res(__('auth.verified_email'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        $user->email_token = generateEmailToken();
        $user->save();

        session()->flash('user_id', $user->id);

        Mail::to($user->email)->bcc(config('mail.admin.address'))->send(new EmailVerification($user));

        return res(__('auth.verify_email', ['email' => $user->email]), [
            'name' => $user->name,
            'email' => $user->email
        ], 201);
    }

    public function mobile($req)
    {
        $validator = Validator::make($req->all(), [
            'name' => 'required|string',
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true) . '|unique:users',
            'password' => 'required|min:6|alpha_num|confirmed'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = new User;
        $user->name = $req->name;
        $user->mobile = $req->mobile;
        $user->password = bcrypt($req->password);
        $user->iso = session('iso', getIso());
        $user->save();

        return $this->sendMobileVerificationToken($req, $user->mobile);
    }

    public function verifyMobile($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $token = $req->token;
        $user = User::where('mobile_token', $token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }
        $user->verified_mobile = 1;
        $user->status = 1;
        $user->mobile_verified_at = now()->toDateTimeString();
        $user->mobile_token = null;
        $user->save();

        return res('Success');
    }

    public function sendMobileVerificationToken($req, $mobile = null)
    {
        // ISO in header is required
        if ($mobile === null) {
            $validator = Validator::make($req->all(), [
                'mobile' => 'required'
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }
            $mobile = $req->mobile;
            $user = User::where('mobile', $mobile)->where('iso', session('iso'))->first();
            if (!$user) {
                return res(__('auth.mobile_not_found'), null, 400);
            }
        }

        $user = User::where('mobile', $mobile)->where('iso', session('iso'))->first();
        if ($user->verified_mobile === 1) {
            return res(__('auth.verified_mobile'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        $token = generateMobileToken();
        $user->mobile_token = $token;
        $user->save();

        (new SMSRepository())->sendRegistrationCode($user);

        return res(__('auth.verify_mobile', ['mobile' => getMobilePrefix($user->iso) . $user->mobile]), [
            'name' => $user->name,
            'mobile' => getMobilePrefix($user->iso) . $user->mobile
        ], 201);
    }

    public function checkEmailAvailability($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email|unique:users'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        return res('Available');
    }

    public function checkMobileAvailability($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true) . '|unique:users',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        return res('Available');
    }
}
