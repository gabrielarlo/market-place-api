<?php

namespace App\Repositories;

use Twilio\Rest\Client;
use App\Interfaces\SMSInterface;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\TwilioException;

class SMSRepository implements SMSInterface
{
    private function _send($to_number, $msg, $imgUrl = 'https://w-blue.website/assets/images/wblue/wblue.png')
    {
        $accountSID = config('twilio.sid');
        $accountToken = config('twilio.token');
        $accountNumber = config('twilio.number');

        $client = new Client($accountSID, $accountToken);

        try {
            $client->messages->create(
                $to_number,
                [
                    'body' => $msg,
                    'from' => $accountNumber,
                    'mediaUrl' => $imgUrl
                ]
            );
            Log::info('Message send from ' . $accountNumber . ' to ' . $to_number);
        } catch (TwilioException $th) {
            Log::error(
                'Could not send SMS Notification.' .
                ' Twilio replied with: ' . $th
            );
        }
    }

    public function sendRegistrationCode($user)
    {
        $to = getMobilePrefix($user->iso) . $user->mobile;
        $msg = 'Your WBlue verification code is: ' . $user->mobile_token;
        $this->_send($to, $msg);
    }

    public function sendSecurityCode($user)
    {
        $to = getMobilePrefix($user->iso) . $user->temp_mobile;
        $msg = 'Your WBlue security code is: ' . $user->mobile_token;
        $this->_send($to, $msg);
    }
}
