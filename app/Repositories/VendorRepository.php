<?php

namespace App\Repositories;

use App\User;
use App\Models\Store;
use App\Models\VendorType;
use App\Models\StoreBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Interfaces\VendorInterface;
use App\Models\SecurityQuestionEntry;
use App\Repositories\AddressRepository;
use App\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Validator;

class VendorRepository implements VendorInterface
{
    public function register($req)
    {
        DB::beginTransaction();
        if (!isset($req->user_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'user_address' => 'required',
            'user_city' => 'required',
            'user_state' => 'required',
            'name' => 'required|unique:stores,name',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            // 'zip' => 'required',
            'country' => 'required|min:3|max:3',
            'company_type' => 'required',
            // 'established_type' => 'required',
            // 'date_established' => 'required',
            // 'paypal_email' => 'required|email',
            // 'bank_account_number' => 'required',
            // 'bank_account_name' => 'required',
            // 'bank_name' => 'required',

            'question_1' => 'required',
            'question_2' => 'required',
            'answer_1' => 'required',
            'answer_2' => 'required',
        ], [
            'name.unique' => 'The Store Name has already been taken',
        ]);
        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $has_store = Store::where('user_id', $req->user_id)->whereIn('is_approved', [0, 1])->where('ban', 0)->count();
        if ($has_store > 0) {
            DB::rollBack();
            return res(__('vendor.has_already'), null, 400);
        }

        $store = new Store;
        $store->name = $req->name;
        $store->address = $req->address;
        $store->city = $req->city;
        $store->state = $req->state;
        $store->country = $req->country;
        $store->zip = '';
        $store->paypal_email = '';
        $store->company_type = $req->company_type;
        $store->established_type = '';
        $store->date_established = '';
        $store->sell_on_country = session('iso');

        if ($req->has('qid')) {
            $store->qid = $req->qid;
        }
        if ($req->has('eid')) {
            $store->eid = $req->eid;
        }
        if ($req->has('commercial_registration')) {
            $store->commercial_registration = $req->commercial_registration;
        }
        if ($req->has('commercial_permit')) {
            $store->commercial_permit = $req->commercial_permit;
        }

        $user_id = (int)$req->user_id;
        if ($user_id == 0) {
            $validator = Validator::make($req->all(), [
                'user_name' => 'required',
                'user_email' => 'required',
                'user_password' => 'required|confirmed|min:6',
            ]);
            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $user_req = new Request();
            $user_req->replace([
                'name' => $req->user_name,
                'email' => $req->user_email,
                'password' => $req->user_password,
                'password_confirmation' => $req->user_password_confirmation,
                'address' => $req->user_address,
                'city' => $req->user_city,
                'state' => $req->user_state,
            ]);
            $ret = (new RegisterRepository)->email($user_req, false);
            if (!session('user_id')) {
                DB::rollBack();
                return $ret;
            }
            $user_id = session('user_id');
        } else {
            $user = User::find($user_id);
            $address_req = new Request();
            $address_req->replace([
                'user_id' => $user_id,
                'name' => $user->name,
                'detailed_address' => $req->user_address,
                'city' => $req->user_city,
                'state' => $req->user_state,
            ]);
            (new AddressRepository)->add($address_req);
        }

        if ($req->has('bank_name') AND strlen($req->bank_name) > 0) {
            $validator = Validator::make($req->all(), [
                'bank_account_number' => 'required',
                'bank_account_name' => 'required',
            ]);
            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $store->bank_account_number = $req->bank_account_number;
            $store->bank_account_name = $req->bank_account_name;
            $store->bank_name = $req->bank_name;
        } else {
            $store->bank_account_number = null;
            $store->bank_account_name = '';
            $store->bank_name = '';
        }

        $store->user_id = $user_id;
        $store->po_box = $req->has('po_box') ? $req->po_box : '';
        $store->zone_number = $req->has('zone_number') ? $req->zone_number : null;
        $store->phone = $req->has('phone') ? $req->phone : '';
        $store->save();

        // TODO: Should be refactored for specific re repository
        $sec = new SecurityQuestionEntry;
        $sec->user_id = $user_id;
        $sec->question = $req->question_1;
        $sec->answer = $req->answer_1;
        $sec->save();
        $sec = new SecurityQuestionEntry;
        $sec->user_id = $user_id;
        $sec->question = $req->question_2;
        $sec->answer = $req->answer_2;
        $sec->save();

        DB::commit();
        return res('Success', $store, 201);
    }

    public function profile($req)
    {
        if (isset($req->vendor_id)) {
            $vendor_id = $req->vendor_id;
        } else {
            $vendor_id = auth()->user()->id;
        }
        $vendor = User::find($vendor_id);
        if (!$vendor) {
            return res(__('vendor.not_found'), null, 400);
        }
        $vendor->makeHidden(['email_verified_at', 'created_at', 'updated_at', 'verified_email', 'status', 'ban', 'verified_mobile', 'mobile_verified_at', 'temp_mobile', 'temp_email']);

        return res('Success', $vendor);
    }

    public function getSecurityQuestions($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }

        $list = SecurityQuestionEntry::where('user_id', $req->user_id)->where('status', 1)->limit(2)->get();

        return res('Success', compact('list'));
    }

    public function updateStore($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', $req->user_id)->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'name' => 'required|unique:stores,name,' . $req->store_id,
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'country' => 'required|min:3|max:3',
            'paypal_email' => 'required|email',
            'company_type' => 'required',
            'date_established' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store = Store::find($req->store_id);
        $store->name = $req->name;
        $store->address = $req->address;
        $store->city = $req->city;
        $store->state = $req->state;
        $store->zip = $req->zip;
        $store->paypal_email = $req->paypal_email;
        $store->company_type = $req->company_type;
        $store->established_type = $req->has('established_type') ? $req->established_type : '';
        $store->date_established = $req->date_established;

        if ($req->has('qid')) {
            // UPLOAD QID
            $store->has_qid = 0;
        }
        if ($req->has('eid')) {
            // UPLOAD EID
            $store->has_eid = 0;
        }
        if ($req->has('commercial_registration')) {
            // UPLOAD REGISTRATION
            $store->has_commercial_registration = 0;
        }
        if ($req->has('commercial_permit')) {
            // UPLOAD PERMIT
            $store->has_commercial_permit = 0;
        }

        if ($req->has('po_box')) {
            $store->po_box = $req->po_box;
        }
        if ($req->has('zone_number')) {
            $store->zone_number = $req->zone_number;
        }
        $store->save();

        return res('Success', $store);
    }

    public function updateBank($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', $req->user_id)->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $store = Store::find($req->store_id);
        if ($req->has('bank_name') && $req->bank_name != '') {
            $validator = Validator::make($req->all(), [
                'bank_name' => 'required',
                'bank_account_number' => 'required',
                'bank_account_name' => 'required',
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }
        }
        if ($req->has('bank_account_number')) {
            $store->bank_account_number = $req->bank_account_number;
        }
        if ($req->has('bank_account_name')) {
            $store->bank_account_name = $req->bank_account_name;
        }
        if ($req->has('bank_name')) {
            $store->bank_name = $req->bank_name;
        }
        $store->save();

        return res('Success', $store);
    }

    public function updateSecurity($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'question_1' => 'required',
            'question_2' => 'required',
            'answer_1' => 'required',
            'answer_2' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $sec = SecurityQuestionEntry::where('user_id', $req->user_id)->limit(2)->get();
        if (!$sec) {
            for ($i=0; $i < 2; $i++) { 
                $sq = new SecurityQuestionEntry;
                $sq->user_id = $req->user_id;
                $sq->question = $req->get('question_' . ($i + 1));
                $sq->answer = $req->get('answer_' . ($i + 1));
                $sq->save();
            }
        } else {
            $i = 1;
            foreach ($sec as $value) {
                $sq = SecurityQuestionEntry::find($value->id);
                if ($sq) {
                    $sq->question = $req->get('question_' . $i);
                    $sq->answer = $req->get('answer_' . $i);
                    $sq->save();
                }
                $i++;
            }
        }

        return res('Success');
    }

    public function getTypes($req)
    {
        $types = VendorType::where('status', 1)->get();
        $types->makeHidden(['status', 'created_at', 'updated_at']);

        return res('Success', $types);
    }

    public function list($req)
    {
        $pending = Store::where('is_approved', 0)->where('ban', 0)->where('is_declined', 0)->get();
        $approved = Store::where('is_approved', 1)->where('ban', 0)->where('is_declined', 0)->orderBy('approved_at', 'desc')->get();
        $declined = Store::where('ban', 0)->where('is_declined', 1)->get();
        $banned = Store::where('ban', 1)->get();

        return res('Success', compact('pending', 'approved', 'declined', 'banned'));
    }

    public function changeStatus($req)
    {
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $role = auth()->user()->type_info;
        if ($role != 'administrator' && $role != 'agent') {
            return res('You are not allowed', null, 400);
        }
        
        if (!$req->has('action') || $req->action == '') {
            return res('Action is Required', null, 400);
        }
        
        $store = Store::find($req->store_id);
        if (!$store) {
            return res('Unable to find store', null, 400);
        }

        switch ($req->action) {
            case 'approve':
                if ($store->is_approved == 1) {
                    return res('Already approved', $store, 200);
                }
                if ($store->ban == 1) {
                    return res('This store is banned', $store, 400);
                }
                $store->is_approved = 1;
                $store->approved_at = now();
                $store->is_declined = 0;
                $store->save();
                return res('Success', $store, 200);
                break;
            case 'decline':
                $store->is_approved = 0;
                $store->is_declined = 1;
                $store->save();
                return res('Success', $store, 200);
                break;
            case 'ban':
                $store->ban = 1;
                $store->save();
                return res('Success', $store, 200);
                break;
            case 'unban':
                $store->ban = 0;
                $store->save();
                return res('Success', $store, 200);
                break;
            
            default:
                return res('Invalid action', null, 400);
                break;
        }
    }

    public function updateBanners($req)
    {
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', auth()->id())->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'banners' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $s = Store::find($req->store_id);
        if (!$s) {
            return res('Invalid Store', null, 400);
        }

        StoreBanner::where('store_id', $req->store_id)->delete();

        foreach ($req->banners as $key => $value) {
            $name = $s->name . ' - ' . encode(now()->timestamp);
            $sb = new StoreBanner;
            $sb->store_id = $req->store_id;
            $sb->base64 = $value;
            $sb->name = $name;
            $sb->save();
        }
        
        return res('Success', null, 200);
    }
}
