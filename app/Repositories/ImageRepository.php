<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductImage;
use App\Interfaces\ImageInterface;
use Illuminate\Support\Facades\Redis;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ImageRepository implements ImageInterface
{
    private function returnImg($path, $width, $height = null)
    {
        if ($height) {
            return Image::make($path)->resize($width, $height)->response();
        } else {
            return Image::make($path)->resize($width, $height, function ($const) {
                $const->aspectRatio();
            })->response();
        }
    }

    public function avatar($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $path = storage_path('/app/public/images/avatars/' . $id . '.png');
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/avatars/' . $id . '.png');
        } else {
            $path = storage_path('/app/public/images/avatar.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function product($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $product = Product::find($id);
        if ($product) {
            $data = Redis::get('images:products:' . $product->uuid);
            if ($data) {
                $images = json_decode($data);
                if (count($images) > 0) {
                    $path = $images[0]->data;
                }
            } else {
                $path = storage_path('/app/public/images/product.png');
            }
        } else {
            $path = storage_path('/app/public/images/product.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function banner($req, $name, $width = 256, $height = null)
    {
        $path = storage_path('/app/public/images/banners/' . $name);
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/banners/' . $name);
        } else {
            $path = storage_path('/app/public/images/banner.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function bannerNames($req)
    {
        $files = Storage::disk('public')->files('/images/banners');
        $names = [];
        foreach ($files as $key => $value) {
            $filename = basename($value);
            array_push($names, $filename);
        }
        return res('Success', $names);
    }

    public function store($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $path = storage_path('/app/public/images/store/' . $id . '.png');
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/store/' . $id . '.png');
        } else {
            $path = storage_path('/app/public/images/store.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function productBase64s($req, $hashid, $index, $width = 400, $height)
    {
        $id = decode($hashid, 'uuid');
        $product = Product::find($id);
        if ($product) {
            $images = ProductImage::where('product_id', $product->id)->get();
            if (count($images) > 0) {
                $path = $images[$index]->base64;
            } else {
                $path = storage_path('/app/public/images/product.png');
            }
        } else {
            $path = storage_path('/app/public/images/product.png');
        }
        return $this->returnImg($path, $width, $height);
    }
}
