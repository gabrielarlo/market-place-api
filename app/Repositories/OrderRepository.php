<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Order;
use App\Models\CartItem;
use App\Models\Inventory;
use App\Models\OrderStore;
use App\Models\PaymentMethod;
use App\Models\OrderStoreItem;
use App\Models\OrderItemTracking;
use App\Interfaces\OrderInterface;
use Illuminate\Support\Facades\Validator;

class OrderRepository implements OrderInterface
{
    public function list($req)
    {

    }

    public function show($req)
    {

    }

    public function paymentMethods($req)
    {
        $methods = PaymentMethod::where('status', 1)->get();
        $methods->makeHidden(['created_at', 'updated_at', 'status']);

        return res('Success', $methods);
    }

    public function place($req) {
        $validator = Validator::make($req->all(), [
            'shipping_rate' => 'required',
            'payment_method_id' => 'required',
            'sub_total' => 'required',
            'selected_shippings' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $order_number = encode(now()->timestamp, 'order-number');

        $order = new Order;
        $order->customer_id = auth()->user()->id;
        $order->order_number = $order_number;
        $order->sub_total = $req->sub_total;
        $order->shipping_rate = $req->shipping_rate;
        $order->payment_method_id = $req->payment_method_id;
        $order->billing_address = json_encode(auth()->user()->default_billing_address);
        $order->shipping_address = json_encode(auth()->user()->default_shipping_address);
        $order->billing_address_coordinates = json_encode(auth()->user()->default_billing_address->latlng);
        $order->shipping_address_coordinates = json_encode(auth()->user()->default_shipping_address->latlng);
        $order->save();

        $this->saveOrderStore($order->id, $req->selected_shippings);

        return res('Success', $order_number, 201);
    }

    private function saveOrderStore(int $order_id, $selected_shippings)
    {
        $carts = Cart::where('user_id', auth()->user()->id)->where('on_checkout', 1)->where('is_deleted', 0)->get();
        foreach ($carts as $value) {
            $sub_total = 0;
            foreach ($value->items as $item) {
                if ($item->product->is_on_sale == 1) {
                    $sub_total += $item->product->sale_price * $item->quantity;
                } else {
                    $sub_total += $item->product->price * $item->quantity;
                }
            }
            $os = new OrderStore;
            $os->order_id = $order_id;
            $os->store_id = $value->store_id;
            $os->sub_total = $sub_total;
            $os->shipping_rate_info = isset($selected_shippings[$value->id]) ? json_encode($selected_shippings[$value->id]) : json_encode([]);
            $os->is_rated = 0;
            $os->save();

            $this->saveOrderStoreItem($value, $os->id);
        }
    }

    private function saveOrderStoreItem(Cart $cart, int $order_store_id)
    {
        foreach ($cart->items as $value) {
            $comm_settings = settings('commission');
            $osi = new OrderStoreItem;
            $osi->order_store_id = $order_store_id;
            $osi->product_id = $value->product_id;
            $osi->name = $value->product->name;
            $osi->price = $value->product->is_on_sale == 1 ? $value->product->sale_price : $value->product->price;
            $osi->quantity = $value->quantity;
            if ($comm_settings->mode == 'percentage') {
                $osi->commission = ($osi->price * $osi->quantity) * ($comm_settings->value / 100);
            } else {
                $osi->commission = $comm_settings->value;
            }
            $osi->is_rated = 0;
            $osi->save();

            $oit = new OrderItemTracking;
            $oit->order_store_item_id = $osi->id;
            $oit->datail = 'Order Created';
            $oit->save();

            $inv = new Inventory;
            $inv->product_id = $osi->product_id;
            $inv->actor_id = auth()->user()->id;
            $inv->quantity = $osi->quantity * -1;
            $inv->remarks = 'Ordered';
            $inv->save();

            $item = CartItem::find($value->id);
            $item->is_ordered = 1;
            $item->save();
        }
    }
}
