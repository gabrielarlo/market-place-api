<?php

namespace App\Repositories;

use App\Models\Address;
use Illuminate\Support\Facades\DB;
use App\Interfaces\AddressInterface;
use Illuminate\Support\Facades\Validator;

class AddressRepository implements AddressInterface
{
    public function list($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $list = Address::where('user_id', $req->user_id)->where('status', 1)->orderBy('is_default', 'DESC')->where('iso', session('iso'))->get();
        $list->makeHidden(['user_id', 'status', 'created_at', 'updated_at']);
        $billing = Address::where('user_id', $req->user_id)->where('status', 1)->where(function ($q) {
            $q->where('address_type', 0)->orWhere('address_type', 2);
        })->where('is_default', 1)->where('iso', session('iso'))->first();
        if ($billing) {
            $billing->makeHidden(['user_id', 'status', 'created_at', 'updated_at', 'is_default']);
        }
        $shipping = Address::where('user_id', $req->user_id)->where('status', 1)->where(function ($q) {
            $q->where('address_type', 1)->orWhere('address_type', 2);
        })->where('is_default', 1)->where('iso', session('iso'))->first();
        if ($shipping) {
            $shipping->makeHidden(['user_id', 'status', 'created_at', 'updated_at', 'is_default']);
        }

        return res('Success', compact('billing', 'shipping', 'list'));
    }

    public function add($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            // 'phone' => 'required',
            'state' => 'required',
            'city' => 'required',
            // 'zip' => 'required',
            'detailed_address' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failes', $validator->errors(), 412);
        }

        $address = new Address;
        $address->user_id = $req->user_id;
        $address->name = $req->name;
        $address->phone = $req->has('phone') ? $req->phone : '';
        $address->state = $req->state;
        $address->city = $req->city;
        $address->zip = $req->has('zip') ? $req->zip : '';
        $address->detailed_address = $req->detailed_address;
        $address->latlng = $req->has('latlng') ? json_encode($req->latlng) : null;
        $address->iso = session('iso');
        $address->save();

        $this->makeThisAsDefault($address, $req);

        return res('Success', $address, 201);
    }

    public function update($req)
    {
        if (!$req->has('id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('user_id')) {
            return res(__('user.not_allowed'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'phone' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'detailed_address' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failes', $validator->errors(), 412);
        }

        $address = Address::find($req->id);
        if (!$address) {
            return res(__('validation.not_found'), null, 400);
        }
        $address->user_id = $req->user_id;
        $address->name = $req->name;
        $address->phone = $req->phone;
        $address->state = $req->state;
        $address->city = $req->city;
        $address->zip = $req->zip;
        $address->detailed_address = $req->detailed_address;
        $address->latlng = json_encode($req->latlng);
        $address->iso = session('iso');

        if ($req->has('address_type')) {
            $address->address_type = (int)$req->address_type;
        }
        if ($req->has('is_default')) {
            $address->is_default = (int)$req->is_default;
        }
        $address->save();

        if ($address->is_default == 1) {
            $addressTable = $address->getTable();
            if ($address->address_type == 2) {
                DB::table($addressTable)->where('id', '<>', $req->id)->where('user_id', $req->user_id)->where('is_default', 1)->where('status', 1)->update(['is_default' => 0]);
            } elseif ($address->address_type == 1) {
                DB::table($addressTable)->where('id', '<>', $req->id)->where('user_id', $req->user_id)->where('address_type', 2)->where('is_default', 1)->where('status', 1)->update(['address_type' => 0]);
                DB::table($addressTable)->where('id', '<>', $req->id)->where('user_id', $req->user_id)->where('address_type', 1)->where('is_default', 1)->where('status', 1)->update(['is_default' => 0]);
            } elseif ($address->address_type == 0) {
                DB::table($addressTable)->where('id', '<>', $req->id)->where('user_id', $req->user_id)->where('address_type', 2)->where('is_default', 1)->where('status', 1)->update(['address_type' => 1]);
                DB::table($addressTable)->where('id', '<>', $req->id)->where('user_id', $req->user_id)->where('address_type', 0)->where('is_default', 1)->where('status', 1)->update(['is_default' => 0]);
            }
        }

        if ($address->address_type < 2) {
            $address_type = $address->address_type == 0 ? 1 : 0;
            $other_default_address = Address::where('user_id', $address->user_id)->where('address_type', $address_type)->where('is_default', 1)->where('status', 1)->first();
            if (!$other_default_address) {
                $other_address = Address::where('user_id', $address->user_id)->where('address_type', $address_type)->where('is_default', 0)->where('status', 1)->first();
                if ($other_address) {
                    $other_address->is_default = 1;
                    $other_address->save();
                } else {
                    $other_address = Address::where('user_id', $address->user_id)->where('address_type', 2)->where('is_default', 0)->where('status', 1)->first();
                    if ($other_address) {
                        $other_address->address_type = $address_type;
                        $other_address->is_default = 1;
                        $other_address->save();
                    }
                }
            }
        }

        return res('Success', $address);
    }

    public function delete($req)
    {
        if (!$req->has('id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('user_id')) {
            return res(__('user.not_allowed'), null, 400);
        }
        $address = Address::find($req->id);
        if (!$address) {
            return res(__('validation.not_found'), null, 400);
        }
        $address->status = 0;
        $address->save();

        return res('Success');
    }

    private function makeThisAsDefault(Address $address, $req)
    {
        $has_default_billing = Address::where('user_id', $req->user_id)->where(function ($q) {
            $q->where('address_type', 0)->orWhere('address_type', 2);
        })->where('is_default', 1)->where('status', 1)->first();
        $has_default_shipping = Address::where('user_id', $req->user_id)->where(function ($q) {
            $q->where('address_type', 1)->orWhere('address_type', 2);
        })->where('is_default', 1)->where('status', 1)->first();

        if (!$has_default_billing) {
            if (!$has_default_shipping) {
                $address->address_type = 2;
                $address->is_default = 1;
                $address->save();
            } else {
                $address->address_type = 0;
                $address->is_default = 1;
                $address->save();
            }
        } else {
            if (!$has_default_shipping) {
                $address->address_type = 1;
                $address->is_default = 1;
                $address->save();
            }
        }
    }
}
