<?php

namespace App\Repositories;

use App\User;
use App\Models\State;
use App\Models\Product;
use App\Models\WishList;
use App\Models\ProductType;
use App\Models\StockStatus;
use App\Models\ProductImage;
use App\Models\ShippingRate;
use App\Models\ProductReview;
use App\Models\ProductCategory;
use App\Models\ProductShipping;
use App\Models\ProductAttribute;
use App\Models\ShippingCategory;
use Illuminate\Support\Facades\DB;
use App\Interfaces\ProductInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;

class ProductRepository implements ProductInterface
{
    public function list($req)
    {
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        if ($req->filter === 'published') {
            $products = Product::where('store_id', $req->store_id)->where('status', 1)->where('is_deleted', 0)->get();
        } elseif ($req->filter === 'unpublished') {
            $products = Product::where('store_id', $req->store_id)->where('status', '<>', 1)->where('is_deleted', 0)->get();
        } else {
            $products = Product::where('store_id', $req->store_id)->where('is_deleted', 0)->get();
        }
        $products->makeHidden(['store_info']);

        return res('Success', $products);
    }

    public function show($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $product = Product::find($req->product_id);
        if (!$product) {
            return res(__('product.not_found'), null, 400);
        }

        return res('Success', $product);
    }

    public function add($req)
    {
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('user_id')) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if ($req->user_id != auth()->user()->id) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'product_type_id' => 'required',
            'category_ids' => 'required',
            'name' => 'required',
            'price' => 'required',
            'short_description' => 'required|max:500',
            'long_description' => 'required',
            'tags' => 'required',
            'delivery_options' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $has_product = Product::where('store_id', $req->store_id)->where('name', $req->name)->count();
        if ($has_product > 0) {
            return res(__('product.existed'), null, 400);
        }

        if (count($req->tags) > 5 && count($req->tags) < 0) {
            return res(__('product.tags'), null, 400);
        }

        $product = new Product;
        $product->store_id = $req->store_id;
        $product->product_type_id = $req->product_type_id;
        $product->category_ids = $req->category_ids;
        $product->name = $req->name;
        $product->price = $req->price;
        $product->short_description = $req->short_description;
        $product->long_description = json_encode($req->long_description);
        // $product->long_description = $req->long_description;
        $product->tags = $req->tags;
        $product->uuid = $req->uuid;

        if ($req->has('sale_price')) {
            if ($req->sale_price > 0) {
                $validator = Validator::make($req->all(), [
                    'sale_date_from' => 'required',
                    'sale_date_to' => 'required',
                ]);
                if ($validator->fails()) {
                    return res('Failed', $validator->errors(), 412);
                }

                $product->sale_price = $req->sale_price;
                $product->sale_date_from = $req->sale_date_from;
                $product->sale_date_to = $req->sale_date_to;
            }
        }

        if ($req->has('stock_status_id')) {
            $product->stock_status_id = $req->stock_status_id;
        }
        if ($req->has('sku')) {
            $product->sku = $req->sku;
        }
        if ($req->has('dimensions')) {
            $product->dimensions = $req->dimensions;
        }
        if ($req->has('weight')) {
            $product->weight = $req->weight;
        }
        if ($req->has('processing_time')) {
            $product->processing_time = $req->processing_time;
        }
        $product->save();

        $this->saveCategoryRelation($product->id, $req->category_ids);
        $this->saveDeliveryDetails($product->id, $req->delivery_options);

        if ($req->has('quantity')) {
            $req->request->add(['product_id' => $product->id]);
            (new InventoryRepository)->add($req);
        }

        if ($req->has('variations')) {
            $this->saveVariations($product->id, $req->variations);
        }

        if ($req->has('images_base64') && count($req->images_base64) > 0) {
            foreach ($req->images_base64 as $key => $value) {
                $pi = new ProductImage;
                $pi->product_id = $product->id;
                $pi->base64 = $value;
                $pi->file_name = $product->name . '-' . $key;
                $pi->save();
            }
        }

        return res('Success', $product, 201);
    }

    public function update($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('user_id')) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if ($req->user_id != auth()->user()->id) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'product_type_id' => 'required',
            'category_ids' => 'required',
            'name' => 'required',
            'price' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'tags' => 'required',
            'delivery_options' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $product = Product::find($req->product_id);
        if (!$product) {
            return res(__('product.not_found'), null, 400);
        }

        $product->store_id = $req->store_id;
        $product->product_type_id = $req->product_type_id;
        $product->category_ids = $req->category_ids;
        $product->name = $req->name;
        $product->price = $req->price;
        $product->short_description = $req->short_description;
        $product->long_description = $req->long_description;
        $product->tags = $req->tags;
        $product->status = $req->status;
        $product->is_requested = $req->is_requested;
        $product->is_deleted = $req->is_deleted;

        if ($req->has('sale_price')) {
            if ($req->sale_price > 0) {
                $validator = Validator::make($req->all(), [
                    'sale_date_from' => 'required',
                    'sale_date_to' => 'required',
                ]);
                if ($validator->fails()) {
                    return res('Failed', $validator->errors(), 412);
                }

                $product->sale_price = $req->sale_price;
                $product->sale_date_from = $req->sale_date_from;
                $product->sale_date_to = $req->sale_date_to;
            }
        }

        if ($req->has('stock_status_id')) {
            $product->stock_status_id = $req->stock_status_id;
        }
        if ($req->has('sku')) {
            $product->sku = $req->sku;
        }
        if ($req->has('dimensions')) {
            $product->dimensions = $req->dimensions;
        }
        if ($req->has('weight')) {
            $product->weight = $req->weight;
        }
        if ($req->has('processing_time')) {
            $product->processing_time = $req->processing_time;
        }
        $product->save();

        $this->saveCategoryRelation($product->id, $req->category_ids);
        $this->saveDeliveryDetails($product->id, $req->delivery_options);

        if ($req->has('variations')) {
            $this->saveVariations($req->product_id, $req->variations, 'update');
        }

        return res('Success', $product);
    }

    private function saveDeliveryDetails($product_id, $delivery_options)
    {
        ProductShipping::where('product_id', $product_id)->update(['status' => 0]);
        foreach ($delivery_options as $value) {
            if ($value['enable'] && $value['category_id']) {
                $ps = ProductShipping::where('product_id', $product_id)->where('shipping_category_id', $value['category_id'])->first();
                if (!$ps) {
                    $ps = new ProductShipping;
                    $ps->product_id = $product_id;
                    $ps->shipping_category_id = $value['category_id'];
                }
                $ps->shipping_id = $value['shipping_id'];
                $ps->only_state_ids = $value['only_ids'];
                $ps->except_state_ids = $value['except_ids'];
                $ps->filter_by = $value['filter_by'];
                $ps->status = 1;
                $ps->save();
            }
        }
    }

    private function saveCategoryRelation(int $product_id, array $category_ids)
    {
        ProductCategory::where('product_id', $product_id)->delete();
        foreach ($category_ids as $id) {
            $pc = new ProductCategory;
            $pc->uid = encode(now()->timestamp + $id, 'entry-id');
            $pc->product_id = $product_id;
            $pc->category_id = $id;
            $pc->save();
        }
    }

    public function saveVariations($product_id, $variations, $transaction = 'add')
    {
        $collection = collect($variations)->pluck('name');
        $table = (new ProductAttribute())->getTable();
        DB::table($table)->where('status', 1)->whereNotIn('name', $collection)->update(['status' => 0]);

        if (count($variations) > 0) {
            foreach ($variations as $value) {
                if ($transaction === 'add') {
                    $name = strtolower($value['name']);
                } else {
                    $name = strtolower($value['old_name']);
                }
                $var = ProductAttribute::where('product_id', $product_id)->where('name', $name)->where('status', 1)->first();
                if (!$var) {
                    $var = new ProductAttribute();
                    $var->product_id = $product_id;
                    $var->name = strtolower($value['name']);
                }
                $var->values = $value['values'];
                $var->save();
            }
        }
    }

    public function uploadImage($req)
    {
        // TODO: Main logic here that returns url
    }

    public function types($req)
    {
        $types = ProductType::where('status', 1)->get();
        $types->makeHidden(['created_at', 'updated_at', 'status']);
        return res('Success', $types);
    }

    public function stockStatus($req)
    {
        $list = StockStatus::where('status', 1)->get()->map(function ($item) {
            $item->name = title_case($item->name);
            return $item;
        });
        $list->makeHidden(['status', 'created_at', 'updated_at']);
        return res('Success', $list);
    }

    public function mostPopular($req)
    {
        $limit = 10;
        if ($req->has('limit')) {
            $limit = $req->limit;
        }
        if ($req->has('user_id') && $req->user_id !== null) {
            Auth::loginUsingId($req->user_id);
        }
        if ($req->has('store_id') && $req->store_id !== null) {
            $list = Product::with('reviews')->where('store_id', $req->store_id)->where('status', 1)->where('is_deleted', 0)->limit($limit)->get()->sortByDesc(function($product) {
                return $product->reviews->sum('rating');
            });
        } else {
            $list = Product::with('reviews')->where('status', 1)->where('is_deleted', 0)->limit($limit)->get()->sortByDesc(function($product) {
                return $product->reviews->sum('rating');
            });
        }
        $collection = collect($list);
        $list->makeHidden(['delivery_options']);
        $collection = $collection->filter(function ($item) {
            // dump(count($item->delivery_options_2));
            return count($item->delivery_options_2) > 0;
        })->values();

        return res(count($collection), $collection);
        // return res('Success', $list);
    }

    public function uploadImages($req)
    {
        if (!$req->has('uuid')) {
            return res('uuid is required', null, 400);
        }
        if (!$req->has('data')) {
            return res('data is required', null, 400);
        }
        if (count(json_decode($req->data)) > 8) {
            return res('Failed. Max of 8 images only', count(json_decode($req->data)), 400);
        }

        Redis::set('images:products:' . $req->uuid, $req->data);

        return res('Success', count(json_decode($req->data)));
    }

    public function getProductPhotos($req)
    {
        if (!$req->has('uuid')) {
            return res('uuid is required', null, 400);
        }
        $data = Redis::get('images:products:' . $req->uuid);
        if (!$data) {
            return res('Success', []);
        }

        return res('Success', json_decode($data));
    }

    public function toggleToFavorite($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'status' => 'required|min:0|max:1'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = auth()->user()->id;
        $wish = WishList::where('user_id', $user_id)->where('product_id', $req->product_id)->first();
        if (!$wish) {
            $wish = new WishList();
            $wish->user_id = $user_id;
            $wish->product_id = $req->product_id;
        }
        $wish->status = $req->status;
        $wish->save();

        return res($wish->status == 1 ? 'Added to WishList' : 'Removed from WishList', (int)$wish->status);
    }

    public function details($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        
        // TODO:: get user location state for $state = all
        $state = 'all';
        if ($req->has('state')) {
            $state = $req->state;
        }
        if (!auth()->user()) {
            if ($req->has('user_id')) {
                Auth::loginUsingId($req->user_id);
            }
        }

        $product = Product::where('id', $req->product_id)->where('status', 1)->where('is_deleted', 0)->first();
        if (!$product) {
            return res(__('validation.not_found'), null, 400);
        }

        $stateObj = State::where('name', $state)->first();
        $delivery_options = deliveryOptions($product->id, $stateObj);
        $product->makeHidden(['delivery_options', 'delivery_options_2', 'store_info']);
        $collection = collect($product);
        $collection->put('delivery_options_2', $delivery_options);

        return res('Success', $collection);
    }

    public function items($req)
    {
        if ($req->has('store_id')) {
            $list = Product::where('store_id', $req->store_id)->where('status', 1)->where('is_deleted', 0)->get();
        } else {
            $list = Product::where('status', 1)->where('is_deleted', 0)->get();
        }
        $list->makeHidden(['store_info', 'delivery_options']);

        return res('Success', $list);
    }

    public function updateStatus($req)
    {
        if (auth()->user()->type_info != 'administrator') {
            return res(__('vendor.not_allowed'), null, 400);
        }
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'action' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        
        $p = Product::find($req->product_id);
        if (!$p) {
            return res('Invalid Product', null, 400);
        }
        if ($req->action == 'post') {
            $p->is_deleted = 0;
            $p->status = 1;
            $p->is_requested = 0;
            $p->save();
            \recordOnProduct($p->id, $req->action, 200);
            return res('Success', $p, 200);
        } elseif ($req->action == 'delete') {
            $p->is_deleted = 1;
            $p->is_requested = 0;
            $p->save();
            \recordOnProduct($p->id, $req->action, 200);
            return res('Success', $p, 200);
        }

        return res('Error! Invalid action', null, 400);
    }

    public function customerReviews($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $list = ProductReview::where('product_id', $req->product_id)->where('status', 1)->get();
        
        return res('Success', $list);
    }
}
