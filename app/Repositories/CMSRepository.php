<?php

namespace App\Repositories;

use App\Models\Cms;
use App\Interfaces\CMSInterface;

class CMSRepository implements CMSInterface
{
    public function getContent($req, $slug)
    {
        $content = Cms::where('slug', $slug)->where('status', 1)->first();
        if (!$content) {
            return res('Not Found', null, 400);
        }

        return res('Success', $content);
    }
}
