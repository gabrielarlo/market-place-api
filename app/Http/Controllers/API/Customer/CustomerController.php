<?php

namespace App\Http\Controllers\API\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CustomerInterface;

class CustomerController extends Controller
{
    protected $req;
    protected $customer;

    public function __construct(Request $req, CustomerInterface $customer)
    {
        $this->req = $req;
        $this->customer = $customer;
    }

    public function list()
    {
        return $this->customer->list($this->req);
    }

    public function show()
    {
        return $this->customer->show($this->req);
    }
}
