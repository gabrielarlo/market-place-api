<?php

namespace App\Http\Controllers\API\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\AddressInterface;

class AddressController extends Controller
{
    protected $req;
    protected $address;

    public function __construct(Request $req, AddressInterface $address)
    {
        $this->req = $req;
        $this->address = $address;
    }

    public function list()
    {
        return $this->address->list($this->req);
    }

    public function add()
    {
        return $this->address->add($this->req);
    }

    public function update()
    {
        return $this->address->update($this->req);
    }

    public function delete()
    {
        return $this->address->delete($this->req);
    }
}
