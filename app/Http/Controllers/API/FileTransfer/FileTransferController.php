<?php

namespace App\Http\Controllers\API\FileTransfer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\FileTransferInterface;

class FileTransferController extends Controller
{
    protected $req;
    protected $file;

    public function  __construct(Request $req, FileTransferInterface $file)
    {
        $this->req = $req;
        $this->file = $file;
    }

    public function upload()
    {
        return $this->file->upload($this->req);
    }

    public function download()
    {
        return $this->file->download($this->req);
    }
}
