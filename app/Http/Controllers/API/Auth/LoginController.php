<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Interfaces\LoginInterface;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    protected $eq;
    protected $login;

    public function __construct(Request $req, LoginInterface $login)
    {
        $this->req = $req;
        $this->login = $login;
    }

    public function email()
    {
        return $this->login->email($this->req);
    }

    public function mobile()
    {
        return $this->login->mobile($this->req);
    }

    public function logout()
    {
        return $this->login->logout($this->req);
    }
}
