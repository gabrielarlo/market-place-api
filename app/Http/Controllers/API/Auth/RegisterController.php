<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RegisterInterface;

class RegisterController extends Controller
{
    private $req;
    private $register;

    public function __construct(Request $req, RegisterInterface $register)
    {
        $this->req = $req;
        $this->register = $register;
    }

    public function email()
    {
        return $this->register->email($this->req);
    }

    public function verifyEmail()
    {
        return $this->register->verifyEmail($this->req);
    }

    public function sendEmailVerificationToken()
    {
        return $this->register->sendEmailVerificationToken($this->req);
    }

    public function mobile()
    {
        return $this->register->mobile($this->req);
    }

    public function verifyMobile()
    {
        return $this->register->verifyMobile($this->req);
    }

    public function sendMobileVerificationToken()
    {
        return $this->register->sendMobileVerificationToken($this->req);
    }
}
