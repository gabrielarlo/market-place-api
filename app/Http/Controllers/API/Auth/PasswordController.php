<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\PasswordInterface;

class PasswordController extends Controller
{
    private $req;
    private $password;

    public function __construct(Request $req, PasswordInterface $password)
    {
        $this->req = $req;
        $this->password = $password;
    }

    public function requestEmail()
    {
        return $this->password->requestEmail($this->req);
    }

    public function change()
    {
        return $this->password->change($this->req);
    }

    public function requestMobile()
    {
        return $this->password->requestMobile($this->req);
    }
}
