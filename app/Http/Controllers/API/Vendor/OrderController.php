<?php

namespace App\Http\Controllers\API\Vendor;

use Illuminate\Http\Request;
use App\Interfaces\OrderInterface;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $req;
    protected $order;

    public function __construct(Request $req, OrderInterface $order)
    {
        $this->req = $req;
        $this->order = $order;
    }

    public function list()
    {
        return $this->order->list($this->req);
    }

    public function show()
    {
        return $this->order->show($this->req);
    }
}
