<?php

namespace App\Http\Controllers\API\Vendor;

use Illuminate\Http\Request;
use App\Interfaces\VendorInterface;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    private $req;
    private $vendor;

    public function __construct(Request $req, VendorInterface $vendor)
    {
        $this->req = $req;
        $this->vendor = $vendor;
    }

    public function register()
    {
        return $this->vendor->register($this->req);
    }

    public function profile()
    {
        return $this->vendor->profile($this->req);
    }

    public function getSecurityQuestions()
    {
        return $this->vendor->getSecurityQuestions($this->req);
    }

    public function updateStore()
    {
        return $this->vendor->updateStore($this->req);
    }

    public function updateBank()
    {
        return $this->vendor->updateBank($this->req);
    }

    public function updateSecurity()
    {
        return $this->vendor->updateSecurity($this->req);
    }

    public function list()
    {
        return $this->vendor->list($this->req);
    }

    public function changeStatus()
    {
        return $this->vendor->changeStatus($this->req);
    }

    public function updateBanners()
    {
        return $this->vendor->updateBanners($this->req);
    }
}
