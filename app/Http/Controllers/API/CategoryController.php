<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;

class CategoryController extends Controller
{
    protected $req;
    protected $category;

    public function __construct(Request $req, CategoryInterface $category)
    {
        $this->req = $req;
        $this->category = $category;
    }

    public function top()
    {
        return $this->category->top($this->req);
    }
}
