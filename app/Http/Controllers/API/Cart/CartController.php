<?php

namespace App\Http\Controllers\API\Cart;

use Illuminate\Http\Request;
use App\Interfaces\CartInterface;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    protected $req;
    protected $cart;

    public function __construct(Request $req, CartInterface $cart)
    {
        $this->req = $req;
        $this->cart = $cart;
    }

    public function list()
    {
        return $this->cart->list($this->req);
    }

    public function add()
    {
        return $this->cart->add($this->req);
    }

    public function count()
    {
        return $this->cart->count($this->req);
    }

    public function clear()
    {
        return $this->cart->clear($this->req);
    }

    public function updateQuantity()
    {
        return $this->cart->updateQuantity($this->req);
    }

    public function toggleCheckout()
    {
        return $this->cart->toggleCheckout($this->req);
    }

    public function remove()
    {
        return $this->cart->remove($this->req);
    }
}
