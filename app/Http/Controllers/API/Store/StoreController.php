<?php

namespace App\Http\Controllers\API\Store;

use Illuminate\Http\Request;
use App\Interfaces\StoreInterface;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    protected $req;
    protected $store;

    public function __construct(Request $req, StoreInterface $store)
    {
        $this->req = $req;
        $this->store = $store;
    }

    public function find()
    {
        return $this->store->find($this->req);
    }

    public function details()
    {
        return $this->store->details($this->req);
    }
}
