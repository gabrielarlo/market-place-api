<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CountryInterface;

class CountryController extends Controller
{
    private $req;
    private $country;

    public function __construct(Request $req, CountryInterface $country)
    {
        $this->req = $req;
        $this->country = $country;
    }

    public function default()
    {
        return $this->country->default($this->req);
    }

    public function list()
    {
        return $this->country->list($this->req);
    }

    public function states()
    {
        return $this->country->states($this->req);
    }
}
