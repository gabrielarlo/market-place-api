<?php

namespace App\Http\Controllers\API\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ProductInterface;

class ProductController extends Controller
{
    protected $req;
    protected $product;

    public function __construct(Request $req, ProductInterface $product)
    {
        $this->req = $req;
        $this->product = $product;
    }

    public function mostPopular()
    {
        return $this->product->mostPopular($this->req);
    }

    public function toggleToFavorite()
    {
        return $this->product->toggleToFavorite($this->req);
    }

    public function details()
    {
        return $this->product->details($this->req);
    }

    public function items()
    {
        return $this->product->items($this->req);
    }

    public function updateStatus()
    {
        return $this->product->updateStatus($this->req);
    }

    public function customerReviews()
    {
        return $this->product->customerReviews($this->req);
    }
}
