<?php

namespace App\Http\Controllers\API\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\InventoryInterface;

class InventoryController extends Controller
{
    protected $req;
    protected $inventory;

    public function __construct(Request $req, InventoryInterface $inventory)
    {
        $this->req = $req;
        $this->inventory = $inventory;
    }

    public function add()
    {
        return $this->inventory->add($this->req);
    }

    public function minus()
    {
        return $this->inventory->minus($this->req);
    }

    public function list()
    {
        return $this->inventory->list($this->req);
    }
}
