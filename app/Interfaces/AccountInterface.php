<?php

namespace App\Interfaces;

interface AccountInterface
{
    public function refreshToken($req);

    public function update($req);

    public function changePassword($req);

    public function checkPassword($req);

    public function requestMobileChange($req);

    public function confirmMobileChange($req);

    public function requestEmailChange($req);

    public function confirmEmailChange($req);

    public function getSecurityQuestions($req);

    public function checkInfo($req);
}
