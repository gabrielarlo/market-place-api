<?php

namespace App\Interfaces;

interface RegisterInterface
{
    public function email($req);

    public function verifyEmail($req);

    public function sendEmailVerificationToken($req, $email = null);

    public function mobile($req);

    public function verifyMobile($req);

    public function sendMobileVerificationToken($req, $mobile = null);

    public function checkEmailAvailability($req);

    public function checkMobileAvailability($req);
}
