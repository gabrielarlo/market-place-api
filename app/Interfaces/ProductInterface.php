<?php

namespace App\Interfaces;

interface ProductInterface
{
    public function list($req);

    public function show($req);

    public function add($req);

    public function update($req);

    public function uploadImage($req);

    public function types($req);

    public function stockStatus($req);

    public function mostPopular($req);

    public function uploadImages($req);

    public function getProductPhotos($req);

    public function toggleToFavorite($req);

    public function details($req);

    public function items($req);

    public function updateStatus($req);

    public function customerReviews($req);
}
