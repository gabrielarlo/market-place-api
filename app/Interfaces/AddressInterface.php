<?php

namespace App\Interfaces;

interface AddressInterface
{
    public function list($req);

    public function add($req);

    public function update($req);

    public function delete($req);
}
