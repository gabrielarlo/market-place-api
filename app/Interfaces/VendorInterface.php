<?php

namespace App\Interfaces;

interface VendorInterface
{
    public function register($req);

    public function profile($req);

    public function getSecurityQuestions($req);

    public function updateStore($req);

    public function updateBank($req);

    public function updateSecurity($req);

    public function getTypes($req);

    public function list($req);
    
    public function changeStatus($req);

    public function updateBanners($req);
}
