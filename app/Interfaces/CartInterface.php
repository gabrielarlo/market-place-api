<?php

namespace App\Interfaces;

interface CartInterface
{
    public function list($req);

    public function add($req);

    public function count($req);

    public function clear($req);

    public function updateQuantity($req);

    public function toggleCheckout($req);

    public function remove($req);
}
