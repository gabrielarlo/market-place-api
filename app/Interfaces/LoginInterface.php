<?php

namespace App\Interfaces;

use App\User;

interface LoginInterface
{
    public function email($req);

    public function mobile($req);

    public function generateToken(User $user);

    public function logout($req);
}
