<?php

namespace App\Interfaces;

interface StoreInterface
{
    public function find($req);

    public function details($req);
}
