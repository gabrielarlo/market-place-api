<?php

namespace App\Interfaces;

interface ImageInterface
{
    public function avatar($req, $hashid, $width = 256, $height = null);

    public function product($req, $hashid, $width = 256, $height = null);

    public function banner($req, $name, $width = 256, $height = null);

    public function store($req, $name, $width = 256, $height = null);

    public function productBase64s($req, $hashid, $index, $width = 400, $height);

    public function bannerNames($req);
}
