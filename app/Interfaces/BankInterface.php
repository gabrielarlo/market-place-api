<?php

namespace App\Interfaces;

interface BankInterface
{
    public function list($req);

    public function convertCurrency($req);
}
