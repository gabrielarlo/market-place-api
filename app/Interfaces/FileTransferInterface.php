<?php

namespace App\Interfaces;

interface FileTransferInterface
{
    public function upload($req);

    public function download($req);
}
