<?php

namespace App\Interfaces;

interface CustomerInterface
{
    public function list($req);

    public function show($req);
}
