<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Interfaces\SMSInterface;

class Tester extends Command
{
    protected $sms;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Testing Only';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SMSInterface $sms)
    {
        parent::__construct();
        $this->sms = $sms;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Processing...');
        // $this->sms->send('+639052361195', 'Kain na tayo');
        // $this->sms->send('+639279223822', 'Kain na tayo');
        // $this->sms->send('+639777694661', 'Kain na tayo');
        $this->line('Processed');
    }
}
