<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $components = [
            'Country',
            'Category',
            'Account',
            'Address',
            'Login',
            'Password',
            'Register',
            'Customer',
            'Inventory',
            'Image',
            'Order',
            'Product',
            'Vendor',
            'Bank',
            'CMS',
            'Shipping',
            'FileTransfer',
            'Store',
            'SMS',
            'Cart',
            'Checkout',
        ];

        foreach ($components as $component) {
            $this->app->bind("App\\Interfaces\\{$component}Interface", "App\\Repositories\\{$component}Repository");
        }
    }
}
