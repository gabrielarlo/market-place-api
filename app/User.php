<?php

namespace App;

use App\Models\Store;
use App\Models\Address;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    protected $appends = ['status_info', 'mobile_prefix', 'type_info', 'store_info', 'default_billing_address', 'default_shipping_address'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_token', 'mobile_token', 'password_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getMobilePrefixAttribute()
    {
        $prefix = null;
        if (isset($this->attributes['iso'])) {
            $prefix = getMobilePrefix(iso2ToIso3($this->attributes['iso'], true));
        }
        return $prefix;
    }

    public function getStatusInfoAttribute()
    {
        $status = [
            'text' => 'unverified',
            'type' => 'info',
        ];
        $verified = $this->attributes['verified_email'] == 1 || $this->attributes['verified_mobile'] == 1;
        if ($this->attributes['ban'] == 1) {
            $status = [
                'text' => 'banned',
                'type' => 'danger',
            ];
        } else {
            if ($this->attributes['status'] == 1 && $verified) {
                $status = [
                    'text' => 'active',
                    'type' => 'success',
                ];
            }
        }

        return $status;
    }

    public function getTypeInfoAttribute()
    {
        $type = 'customer';
        if ($this->attributes['type'] == 0) {
            $store = Store::where('user_id', $this->attributes['id'])->where('is_approved', 1)->where('ban', 0)->count();
            if ($store > 0) {
                $type = 'vendor';
            } else {
                $store = Store::where('user_id', $this->attributes['id'])->where('is_approved', 0)->where('ban', 0)->count();
                if ($store > 0) {
                    $type = 'prospect';
                }
            }
        } elseif ($this->attributes['type'] == 1) {
            $type = 'agent';
        } elseif ($this->attributes['type'] == 9) {
            $type = 'administrator';
        }
        
        return $type;
    }

    public function getStoreInfoAttribute()
    {
        $info = null;
        $store = Store::where('user_id', $this->attributes['id'])->where('is_approved', 1)->where('ban', 0)->first();
        if ($store) {
            $info = $store;
        }
        return $info;
    }

    public function getDefaultBillingAddressAttribute()
    {
        $billing = Address::where('is_default', 1)->where('address_type', '<>', 1)->where('status', 1)->first();
        return $billing;
    }

    public function getDefaultShippingAddressAttribute()
    {
        $shipping = Address::where('is_default', 1)->where('address_type', '<>', 0)->where('status', 1)->first();
        return $shipping;
    }
}
