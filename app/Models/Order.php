<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'billing_address_coordinates' => 'array',
        'shipping_address_coordinates' => 'array',
    ];
}
