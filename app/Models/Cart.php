<?php

namespace App\Models;

use App\Models\Store;
use App\Models\CartItem;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $appends = ['store_info', 'items_info', 'group_delivery_options'];

    public function items()
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }

    public function getStoreInfoAttribute()
    {
        $store = Store::find($this->attributes['store_id']);
        if ($store) {
            $store->makeHidden(['bank_account_number', 'bank_account_number', 'bank_name', 'created_at', 'updated_at', 'is_approved', 'ban', 'qid', 'eid', 'commercial_registration', 'commercial_permit', 'sell_on_country']);
        }
        return $store;
    }

    public function getItemsInfoAttribute()
    {
        $items = CartItem::where('cart_id', $this->attributes['id'])->where('is_deleted', 0)->where('is_ordered', 0)->get();
        $items->makeHidden(['cart_id', 'is_deleted', 'created_at', 'updated_at']);

        return $items;
    }

    public function getGroupDeliveryOptionsAttribute()
    {
        $items = CartItem::where('cart_id', $this->attributes['id'])->where('is_deleted', 0)->where('is_ordered', 0)->get();
        $delivery_options = [];
        foreach ($items as $item) {
            if (count($delivery_options) > 0) {
                $names = array_column($delivery_options, 'sc_name');
                foreach ($delivery_options as $key => $option) {
                    foreach ($item->product_info->delivery_options_2 as $option_2) {
                        if ($option['sc_name'] == $option_2['sc_name']) {
                            if ($option['min_days'] > $option_2['min_days']) {
                                $delivery_options[$key]['min_days'] = $option_2['min_days'];
                            }
                            if ($option['max_days'] < $option_2['max_days']) {
                                $delivery_options[$key]['max_days'] = $option_2['max_days'];
                            }
                            if ($option['rate'] < $option_2['rate']) {
                                $delivery_options[$key]['rate'] = $option_2['rate'];
                                $delivery_options[$key]['shipping_rate_id'] = $option_2['shipping_rate_id'];
                                $delivery_options[$key]['shipping_id'] = $option_2['shipping_id'];
                            }
                        }
                        if (!in_array($option_2['sc_name'], $names)) {
                            array_push($delivery_options, $option_2);
                        }
                    }
                }
            } else {
                $delivery_options = $item->product_info->delivery_options_2;
            }
        }

        return $delivery_options;
    }
}
