<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $appends = ['city_info'];

    public function getCityInfoAttribute()
    {
        $id = $this->attributes['id'];
        $cities = $this->where('state_id', $id)->where('iso', session('iso'))->where('status', 1)->orderBy('name', 'ASC')->pluck('name');
        return $cities;
    }
}
