<?php

namespace App\Models;

use App\Models\State;
use App\Models\Product;
use App\Models\StoreBanner;
use App\Models\StoreRating;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $appends = ['hashid', 'status_info', 'ratings', 'ratings', 'chat_performance', 'ship_out_time', 'cancellation_rate', 'product_count', 'categories', 'banners'];

    public function products()
    {
        return $this->hasMany(Product::class, 'store_id');
    }

    public function getHashidAttribute()
    {
        $id = $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getStatusInfoAttribute()
    {
        $status = isset($this->attributes['is_approved']) ? $this->attributes['is_approved'] : 0;
        $ban = isset($this->attributes['ban']) ? $this->attributes['ban'] : 0;
        $is_declined = isset($this->attributes['is_declined']) ? $this->attributes['is_declined'] : 0;
        if ($ban == 1) {
            return [
                'type' => 'danger',
                'text' => 'banned'
            ];
        }
        if ($is_declined == 1) {
            return [
                'type' => 'muted',
                'text' => 'declined'
            ];
        }
        if ($status === 0) {
            return [
                'type' => 'info',
                'text' => 'pending'
            ];
        } elseif ($status === 1) {
            return [
                'type' => 'success',
                'text' => 'approved'
            ];
        }
    }

    public function getRatingsAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = StoreRating::where('store_id', $id)->where('status', 1)->count();
        $sum = StoreRating::where('store_id', $id)->where('status', 1)->sum('rating');
        return [
            'count' => $count,
            'average' => $count > 0 ? ($sum / $count) : 0
        ];
    }

    public function getStateInfoAttribute()
    {
        return State::where('name', $this->attributes['state'])->first();
    }

    public function getChatPerformanceAttribute()
    {
        // TODO: Chat Performance
        return  0;
    }

    public function getShipOutTimeAttribute()
    {
        // TODO: Ship out time
        return  [
            'rate' => 'Fast',
            'days' => 2
        ];
    }

    public function getCancellationRateAttribute()
    {
        // TODO: Cancellation rate
        return  0;
    }

    public function getProductCountAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return Product::where('store_id', $id)->where('status', 1)->count();
    }

    public function getCategoriesAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $categories = null;

        $category_ids = Product::where('store_id', $id)->pluck('category_ids');
        $category_ids = array_collapse($category_ids);
        $category_ids = array_unique($category_ids);
        $categories = Category::where('parent_id', 0)->where('status', 1)->whereIn('id', $category_ids)->get();
        $categories->makeHidden(['parent_id', 'created_at', 'updated_at']);

        return $categories;
    }

    public function getBannersAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $banners = StoreBanner::where('store_id', $id)->get();
        return $banners->pluck('base64');
    }
}
