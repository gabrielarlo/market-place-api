<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $appends = ['sub_categories'];

    public function getSubCategoriesAttribute()
    {
        $id = $this->attributes['id'];
        $subs = $this->where('parent_id', $id)->get();
        $subs->makeHidden(['status', 'parent_id', 'created_at', 'updated_at', 'sub_categories']);
        return $subs;
    }
}
