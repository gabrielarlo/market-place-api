<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use App\Models\State;
use App\Models\Store;
use App\Models\Category;
use App\Models\Shipping;
use App\Models\WishList;
use App\Models\Inventory;
use App\Models\ProductType;
use App\Models\ProductImage;
use App\Models\ProductReview;
use App\Models\ProductShipping;
use App\Models\ProductAttribute;
use App\Models\ShippingCategory;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $casts = [
        'tags' => 'array',
        'dimensions' => 'array',
        'category_ids' => 'array',
    ];

    protected $appends = [
        'hashid', 
        'product_type_info', 
        'category_info', 
        'shipping_info', 
        'dimensions_info', 
        'inventory_info', 
        'variations_info', 
        'status_info', 
        'rating_info', 
        'is_favorite', 
        'is_on_sale', 
        'delivery_options', 
        'delivery_options_2', 
        'store_info',
        'favorite_count',
        'image_count',
    ];

    public function reviews()
    {
        return $this->hasMany(ProductReview::class, 'product_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function getHashidAttribute()
    {
        return encode(isset($this->attributes['id']) ? $this->attributes['id'] : 0, 'uuid');
    }

    public function getProductTypeInfoAttribute()
    {
        $type = ProductType::where('id', $this->attributes['product_type_id'])->where('status', 1)->select(['id', 'name'])->first();
        return $type->name;
    }

    public function getCategoryInfoAttribute()
    {
        $ids = $this->attributes['category_ids'];
        $ids = stringToArray($ids);
        $types = Category::whereIn('id', $ids)->where('status', 1)->pluck('name')->toArray();
        return $types;
    }

    public function getShippingInfoAttribute()
    {
        // FIXME: update later
        // $ids = $this->attributes['shipping_ids'];
        // $ids = stringToArray($ids);
        // $shippings = Shipping::whereIn('id', $ids)->where('status', 1)->get(['name', 'rate']);
        // return $shippings;
        return [];
    }

    public function getDimensionsInfoAttribute()
    {
        $data = $this->attributes['dimensions'];
        $data = stringToArray($data);
        $dimensions = [
            'L' => $data[0] ?? '0',
            'W' => $data[1] ?? '0',
            'H' => $data[2] ?? '0',
        ];
        return $dimensions;
    }

    public function getInventoryInfoAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $total = Inventory::where('product_id', $id)->where('status', 1)->sum('quantity');
        return $total;
    }

    public function getVariationsInfoAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $variations = ProductAttribute::where('product_id', $id)->get();
        $variations->makeHidden(['product_id', 'created_at', 'updated_at', 'status']);
        return $variations;
    }

    public function getStatusInfoAttribute()
    {
        if (isset($this->attributes['status']) && isset($this->attributes['is_deleted'])) {
            $status = $this->attributes['status'];
            $is_deleted = $this->attributes['is_deleted'];
            if ($status == 0) {
                if ($is_deleted == 0) {
                    return [
                        'text' => 'pending',
                        'type' => 'info'
                    ];
                } else {
                    return [
                        'text' => 'deleted',
                        'type' => 'danger'
                    ];
                }
            } else {
                if ($is_deleted == 0) {
                    return [
                        'text' => 'posted',
                        'type' => 'success'
                    ];
                } else {
                    return [
                        'text' => 'deleted',
                        'type' => 'danger'
                    ];
                }
            }
        } else {
            return [
                'text' => 'pending',
                'type' => 'info'
            ];
        }
    }

    public function getRatingInfoAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $rate_count = ProductReview::where('product_id', $id)->count();
        if ($rate_count === 0) {
            return 0;
        }
        $ratings = ProductReview::where('product_id', $id)->sum('rating');
        $ratings = $ratings / $rate_count;

        return number_format($ratings, 1);
    }

    public function getIsFavoriteAttribute()
    {
        $fav = 0;
        $user_id = auth()->user() ? auth()->user()->id : 0;
        $product_id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $found = WishList::where('user_id', $user_id)->where('product_id', $product_id)->where('status', 1)->count();
        if ($found > 0) {
            $fav = 1;
        }
        return $fav;
    }

    public function getIsOnSaleAttribute()
    {
        $on_sale = 0;
        if (isset($this->attributes['sale_date_from']) && isset($this->attributes['sale_date_to'])) {
            $sale_date_from = $this->attributes['sale_date_from'];
            $sale_date_to = $this->attributes['sale_date_to'];
            $now = now();

            $sale_date_from = Carbon::parse($sale_date_from, config('app.timezone'));
            $sale_date_to = Carbon::parse($sale_date_to, config('app.timezone'));

            if ($now->lt($sale_date_to) && $now->gt($sale_date_from)) {
                $on_sale = 1;
            }
        }

        return $on_sale;
    }

    public function getImagesDataAttribute()
    {
        $images = [];
        if (isset($this->attributes['uuid'])) {
            $uuid = $this->attributes['uuid'];
            $data = Redis::get('images:products:' . $uuid);
            if ($data) {
                $images = json_decode($data);
            }
        }
        return $images;
    }

    public function getDeliveryOptionsAttribute() {
        $product_id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $shipping_categories = ShippingCategory::all();
        $options = [];
        foreach ($shipping_categories as $category) {
            $ps = ProductShipping::where('product_id', $product_id)->where('shipping_category_id', $category->id)->where('status', 1)->first();
            if ($ps) {
                array_push($options, [
                    'enable' => true,
                    'category_id' => $ps->shipping_category_id,
                    'shipping_id' => $ps->shipping_id,
                    'except_ids' => $ps->except_state_ids,
                    'only_ids' => $ps->only_state_ids,
                    'filter_by' => $ps->filter_by
                ]);
            } else {
                array_push($options, [
                    'enable' => false,
                    'category_id' => null,
                    'shipping_id' => null,
                    'except_ids' => [],
                    'only_ids' => [],
                    'filter_by' => 0
                ]);
            }
        }
        return $options;
    }

    public function getDeliveryOptions2Attribute()
    {
        if (!auth() && !auth()->user()) {
            return [];
        }

        if (!auth() || !auth()->user()) {
            return [];
        }
        $customer = User::find(auth()->user()->id);
        if (!$customer) {
            return [];
        }
        $state = State::where('name', $customer->default_shipping_address->state)->first();
        $product_id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $delivery_options = deliveryOptions($product_id, $state);

        return $delivery_options;
    }

    public function getStoreInfoAttribute()
    {
        $store_id = $this->attributes['store_id'];
        $store = Store::find($store_id);
        $store->makeHidden(['paypal_email', 'bank_account_number', 'bank_account_name', 'bank_name', 'qid', 'eid', 'commercial_registration', 'commercial_permit']);
        return $store;
    }

    public function getFavoriteCountAttribute()
    {
        $product_id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = WishList::where('product_id', $product_id)->where('status', 1)->count();
        return $count;
    }

    public function getImageCountAttribute()
    {
        $product_id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = ProductImage::where('product_id', $product_id)->count();
        return $count;
    }
}
