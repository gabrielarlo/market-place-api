<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $primaryKey = 'scope';
    protected $casts = [
        'value' => 'array'
    ];
}
