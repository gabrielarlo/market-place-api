<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Shipping;
use App\Models\ShippingCategory;
use Illuminate\Database\Eloquent\Model;

class ProductShipping extends Model
{
    protected $casts = [
        'only_state_ids' => 'array',
        'except_state_ids' => 'array',
    ];

    // filter_by: 0 = except, 1 = only

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function shipping_category()
    {
        return $this->belongsTo(ShippingCategory::class, 'shipping_category_id');
    }
    public function shipping()
    {
        return $this->belongsTo(Shipping::class, 'shipping_id');
    }
}
