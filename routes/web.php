<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return res('WEB Server V1 is UP');
});

Route::group(['namespace' => 'WEB'], function () {
    Route::group(['namespace' => 'Image'], function () {
        Route::get('avatar/{hashid}/{width?}/{height?}', 'ImageController@avatar');
        Route::get('product/{hashid}/{width?}/{height?}', 'ImageController@product');
        Route::get('banner/{name}/{width?}/{height?}', 'ImageController@banner');
        Route::get('store/{hashid}/{width?}/{height?}', 'ImageController@store');

        Route::get('product-base-64s/{hashid}/{index}/{width?}/{height?}', 'ImageController@productBase64s');
    });
});
