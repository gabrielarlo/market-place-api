<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// OPEN ROUTE
Route::group(['namespace' => 'API'], function () {
    Route::get('/', function () {
        return res('API Server V1 is UP');
    });

    Route::get('/unauthenticated', function () {
        return res(__('auth.unauthenticated'), null, 401);
    })->name('unauthenticated');

    // OPEN
    Route::group([], function () {
        Route::get('/get-default-country', 'CountryController@default');
        Route::get('/get-countries', 'CountryController@list');
        Route::get('/get-states', 'CountryController@states');
        Route::get('/get-top-categories', 'CategoryController@top');
        Route::get('/get-vendor-types', 'OpenController@vendorTypes');
        Route::get('/get-security-question', 'OpenController@securityQuestions');
        Route::get('/get-product-types', 'OpenController@productTypes');
        Route::get('/get-categories', 'OpenController@categories');
        Route::get('/get-banks', 'OpenController@banks');
        Route::get('/get-vendor-terms-and-conditions', 'OpenController@getVendorTermsAndConditions');
        Route::get('/get-about-us', 'OpenController@getAboutUs');
        Route::get('/get-buyer-protection', 'OpenController@getBuyerProtection');
        Route::get('/get-shipping-providers', 'OpenController@getShippingProviders');
        Route::get('/get-stock-statuses', 'OpenController@getStockStatus');
        Route::get('/get-privacy-policy', 'OpenController@getPrivacyPolicy');
        Route::get('/get-banner-names', 'OpenController@getBannerNames');
        Route::get('/get-payment-methods', 'OpenController@getPaymentMethods');

        Route::post('/check-email-availability', 'OpenController@checkEmailAvailability');
        Route::post('/check-mobile-availability', 'OpenController@checkMobileAvailability');
        Route::post('/get-product-photos', 'OpenController@getProductPhotos');
        Route::post('/convert-currency', 'OpenController@convertCurrency');

        Route::group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function () {
            Route::post('/register', 'VendorController@register');
        });

        Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () {
            Route::post('/most-popular', 'ProductController@mostPopular');
            Route::post('/details', 'ProductController@details');
            Route::post('/items', 'ProductController@items');
        });

        Route::group(['prefix' => 'file-transfer', 'namespace' => 'FileTransfer'], function () {
            Route::post('/upload', 'FileTransferController@upload');
            Route::post('/download', 'FileTransferController@download');
        });

        Route::group(['prefix' => 'store', 'namespace' => 'Store'], function () {
            Route::post('/find', 'StoreController@find');
            Route::post('/details', 'StoreController@details');
        });
    });

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::group(['prefix' => 'register'], function () {
            Route::post('/email', 'RegisterController@email');
            Route::post('/mobile', 'RegisterController@mobile');

            Route::post('/verify-email', 'RegisterController@verifyEmail');
            Route::post('/verify-mobile', 'RegisterController@verifyMobile');

            Route::post('/resend-email-token', 'RegisterController@sendEmailVerificationToken');
            Route::post('/resend-mobile-token', 'RegisterController@sendMobileVerificationToken');
        });

        Route::group(['prefix' => 'login'], function () {
            Route::post('/email', 'LoginController@email');
            Route::post('/mobile', 'LoginController@mobile');
        });

        Route::group(['prefix' => 'request-password-token'], function () {
            Route::post('/request-email', 'PasswordController@requestEmail');
            Route::post('/request-mobile', 'PasswordController@requestMobile');

            Route::post('/change', 'PasswordController@change');
        });
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/check-authentication', function () {
            if (auth() && auth()->user()) {
                $u = auth()->user();
                $user = [
                    'user_id' => $u->id,
                    'hashid' => encode($u->id, 'uuid'),
                    'email' => $u->email,
                    'mobile_prefix' => $u->mobile_prefix,
                    'mobile' => $u->mobile,
                    'name' => $u->name,
                    'type_info' => $u->type_info,
                    'gender' => $u->gender,
                    'bdate' => $u->bdate,
                    'status_info' => $u->status_info,
                ];
                // NOTE: Also update on LoginRepository
                return res('Authenticated', compact('user'));
            }
        });

        Route::group(['namespace' => 'Auth'], function () {
            Route::post('/logout', 'LoginController@logout');
        });
        
        Route::group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function () {
            Route::group(['prefix' => 'profile'], function () {
                Route::post('/show', 'VendorController@profile');
                Route::post('/get-security-questions', 'VendorController@getSecurityQuestions');
                Route::post('/update-store', 'VendorController@updateStore');
                Route::post('/update-bank', 'VendorController@updateBank');
                Route::post('/update-security', 'VendorController@updateSecurity');

                Route::post('/change-status', 'VendorController@changeStatus');
                Route::post('/update-banners', 'VendorController@updateBanners');
            });

            Route::group(['prefix' => 'product'], function () {
                Route::post('/list', 'ProductController@list');
                Route::post('/show', 'ProductController@show');
                Route::post('/add', 'ProductController@add');
                Route::post('/update', 'ProductController@update');
                Route::post('/upload-images', 'ProductController@uploadImages');
            });

            Route::group(['prefix' => 'order'], function () {
                Route::post('/list', 'OrderController@list');
                Route::post('/show', 'OrderController@show');
            });
        });

        Route::group(['prefix' => 'inventory', 'namespace' => 'Product'], function () {
            Route::post('/add', 'InventoryController@add');
            Route::post('/minus', 'InventoryController@minus');
            Route::post('/list', 'InventoryController@list');
        });

        Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () {
            Route::post('/toggle-to-favorite', 'ProductController@toggleToFavorite');
            Route::post('/customer-reviews', 'ProductController@customerReviews');
        });

        Route::group(['prefix' => 'customer', 'namespace' => 'Customer'], function () {
            Route::post('/list', 'CustomerController@list');
            Route::post('/show', 'CustomerController@show');
        });

        Route::group(['prefix' => 'account', 'namespace' => 'Account'], function () {
            Route::post('/update', 'AccountController@update');
            Route::post('/change-password', 'AccountController@changePassword');
            Route::post('/check-password', 'AccountController@checkPassword');
            Route::post('/request-mobile-change', 'AccountController@requestMobileChange');
            Route::post('/confirm-mobile-change', 'AccountController@confirmMobileChange');
            Route::post('/request-email-change', 'AccountController@requestEmailChange');
            Route::post('/confirm-email-change', 'AccountController@confirmEmailChange');

            Route::post('/refresh-token', 'AccountController@refreshToken');

            Route::group(['prefix' => 'address'], function () {
                Route::post('/list', 'AddressController@list');
                Route::post('/add', 'AddressController@add');
                Route::post('/update', 'AddressController@update');
                Route::post('/delete', 'AddressController@delete');
            });

            Route::post('/check-info', 'AccountController@checkInfo');
        });

        Route::group(['prefix' => 'shipping', 'namespace' => 'Shipping'], function () {
            Route::post('/list', 'ShippingController@list');
            Route::post('/categories', 'ShippingController@categories');
        });

        Route::group(['prefix' => 'cart', 'namespace' => 'Cart'], function () {
            Route::post('/list', 'CartController@list');
            Route::post('/add', 'CartController@add');
            Route::post('/count', 'CartController@count');
            Route::post('/clear', 'CartController@clear');
            Route::post('/update-quantity', 'CartController@updateQuantity');
            Route::post('/toggle-checkout', 'CartController@toggleCheckout');
            Route::post('/remove', 'CartController@remove');
        });

        Route::group(['prefix' => 'checkout', 'namespace' => 'Checkout'], function () {
            Route::post('/list', 'CheckoutController@list');
            Route::post('/place-order', 'CheckoutController@placeOrder');
        });

        Route::group(['prefix' => 'manager'], function () {
            Route::group(['prefix' => 'stores', 'namespace' => 'Vendor'], function () { 
                Route::post('/list', 'VendorController@list');
            });

            Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () { 
                Route::post('/update-status', 'ProductController@updateStatus');
            });
        });
    });
});
