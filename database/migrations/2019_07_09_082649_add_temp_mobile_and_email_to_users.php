<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTempMobileAndEmailToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'temp_mobile')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('temp_mobile')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('users', 'temp_email')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('temp_email')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
