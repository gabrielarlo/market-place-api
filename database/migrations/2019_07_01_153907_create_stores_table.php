<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->text('address');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('paypal_email');
            $table->string('company_type');
            $table->string('established_type');
            $table->string('date_established');

            $table->integer('has_qid')->default(0);
            $table->integer('has_eid')->default(0);
            $table->integer('has_commercial_registration')->default(0);
            $table->integer('has_commercial_permit')->default(0);

            $table->integer('po_box')->default(null)->nullable();
            $table->integer('zone_number')->default(null)->nullable();
            $table->integer('bank_account_number')->default(null)->nullable();
            $table->integer('bank_account_name')->default(null)->nullable();
            $table->integer('bank_name')->default(null)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
