<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id')->unsigned();
            $table->string('order_number');
            $table->double('sub_total');
            $table->double('shipping_rate');
            $table->integer('shipping_id')->unsigned();
            $table->text('billing_address');
            $table->text('shipping_address');
            $table->json('billing_address_coordinates');
            $table->json('shipping_address_coordinates');
            $table->integer('payment_method_id')->unsigned();
            $table->double('commission');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
