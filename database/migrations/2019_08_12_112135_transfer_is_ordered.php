<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferIsOrdered extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('carts', 'is_ordered')) {
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('is_ordered');
            });
        }
        if (!Schema::hasColumn('cart_items', 'is_ordered')) {
            Schema::table('cart_items', function (Blueprint $table) {
                $table->integer('is_ordered')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            //
        });
    }
}
