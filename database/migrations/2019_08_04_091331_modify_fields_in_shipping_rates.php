<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFieldsInShippingRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('shipping_rates', 'product_id')) {
            Schema::table('shipping_rates', function (Blueprint $table) {
                $table->dropColumn('product_id');
            });
        }
        if (Schema::hasColumn('shipping_rates', 'state_id')) {
            Schema::table('shipping_rates', function (Blueprint $table) {
                $table->dropColumn('state_id');
            });
        }
        if (Schema::hasColumn('shipping_rates', 'shipping_category_id')) {
            Schema::table('shipping_rates', function (Blueprint $table) {
                $table->dropColumn('shipping_category_id');
            });
        }
        if (!Schema::hasColumn('shipping_rates', 'state_from_id')) {
            Schema::table('shipping_rates', function (Blueprint $table) {
                $table->integer('state_from_id')->unsigned();
            });
        }
        if (!Schema::hasColumn('shipping_rates', 'state_to_id')) {
            Schema::table('shipping_rates', function (Blueprint $table) {
                $table->integer('state_to_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_rates', function (Blueprint $table) {
            //
        });
    }
}
