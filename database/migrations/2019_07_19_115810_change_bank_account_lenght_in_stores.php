<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBankAccountLenghtInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('stores', 'bank_account_number')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->bigInteger('bank_account_number')->change();
            });
        }
        if (Schema::hasColumn('stores', 'bank_account_name')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('bank_account_name')->change();
            });
        }
        if (Schema::hasColumn('stores', 'bank_name')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('bank_name')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
