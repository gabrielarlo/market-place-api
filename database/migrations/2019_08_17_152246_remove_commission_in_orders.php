<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCommissionInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('orders', 'commission')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('commission');
            });
        }
        if (Schema::hasColumn('orders', 'shipping_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('shipping_id');
            });
        }
        if (Schema::hasColumn('orders', 'status')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->integer('status')->default(1)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
