<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalePriceDefaultInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('products', 'sale_price')) {
            Schema::table('products', function (Blueprint $table) {
                $table->float('sale_price')->default(0)->change();
            });
        }
        if (Schema::hasColumn('products', 'sale_date_from')) {
            Schema::table('products', function (Blueprint $table) {
                $table->string('sale_date_from')->nullable()->default(null)->change();
            });
        }
        if (Schema::hasColumn('products', 'sale_date_to')) {
            Schema::table('products', function (Blueprint $table) {
                $table->string('sale_date_to')->nullable()->default(null)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
