<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store_id')->unsigned();
            $table->integer('product_type_id');
            $table->integer('category_id');
            $table->string('name');
            $table->double('price');
            $table->double('sale_price');
            $table->string('sale_date_from');
            $table->string('sale_date_to');
            $table->text('short_description');
            $table->text('long_description');
            $table->json('tags');

            $table->string('sku')->default(null)->nullable();
            $table->integer('stock_status_id');
            $table->json('shipping_ids');
            $table->json('dimensions'); // L,W,H
            $table->double('weight');
            $table->integer('processing_time')->default(1); // day

            $table->integer('status')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->integer('is_featured')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
