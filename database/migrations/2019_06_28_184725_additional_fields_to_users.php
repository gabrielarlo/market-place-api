<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'verified_email')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('verified_email')->default(0);
            });
        }
        if (!Schema::hasColumn('users', 'status')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('status')->default(0);
            });
        }
        if (!Schema::hasColumn('users', 'ban')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('ban')->default(0);
            });
        }
        if (!Schema::hasColumn('users', 'verified_mobile')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('verified_mobile')->default(0);
            });
        }
        if (!Schema::hasColumn('users', 'mobile_verified_at')) {
            Schema::table('users', function (Blueprint $table) {
                $table->timestamp('mobile_verified_at')->default(null)->nullable();
            });
        }
        if (!Schema::hasColumn('users', 'mobile')) {
            Schema::table('users', function (Blueprint $table) {
                $table->timestamp('mobile')->default(null)->nullable();
            });
        }
        if (!Schema::hasColumn('users', 'iso')) {
            Schema::table('users', function (Blueprint $table) {
                $table->timestamp('iso')->default(null)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
