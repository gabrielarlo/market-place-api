<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('product_shippings')) {
            Schema::create('product_shippings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('product_id')->unsigned();
                $table->integer('shipping_category_id')->unsigned();
                $table->integer('shipping_id')->unsigned();
                $table->json('only_state_ids');
                $table->json('except_state_ids');
                $table->integer('filter_by'); //0-all | 1-only | 2-except
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_shippings');
    }
}
