<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferOnCheckout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('carts', 'on_checkout')) {
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('on_checkout');
            });
        }
        if (!Schema::hasColumn('cart_items', 'on_checkout')) {
            Schema::table('cart_items', function (Blueprint $table) {
                $table->integer('on_checkout')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart', function (Blueprint $table) {
            //
        });
    }
}
