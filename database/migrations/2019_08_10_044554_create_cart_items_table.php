<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cart_items')) {
            Schema::create('cart_items', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('cart_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->double('voucher_value')->nullable()->default(null);
                $table->string('voucher_type')->nullable()->default(null);
                $table->json('voucher_condition')->nullable()->default(null);
                $table->integer('quantity')->default(0);
                $table->integer('is_deleted')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}
