<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SeedCountryList::class);
        $this->call(SeedProductTypes::class);
        $this->call(SeedDefaultShipping::class);
        $this->call(SeedPaymentMethods::class);
        $this->call(SeedDefaultSettings::class);
        $this->call(SeedStatesCities::class);
        $this->call(SeedDefaultVendorTypes::class);
        $this->call(SeedSecurityQuestions::class);
        $this->call(SeedCategories::class);
        $this->call(SeedQatarBanks::class);
        $this->call(SeedShippingCategories::class);
        $this->call(SeedShippingRates::class);
    }
}
