<?php

use App\Models\VendorType;
use Illuminate\Database\Seeder;

class SeedDefaultVendorTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!VendorType::where('slug', 'wbmall')->first()) {
            VendorType::firstOrCreate(['slug' => 'wbmall', 'name' => 'W-BLUE MALL VENDOR', 'short_description' => 'WBMall offers to consumers superior shopping experience with 100% authenticity promise', 'long_description' => '', 'details' => "['You are a branch owner or authorized distributor', 'Exclusive access to dedicated WBMall campaigns']"]);
        }
        if (!VendorType::where('slug', 'local')->first()) {
            VendorType::firstOrCreate(['slug' => 'local', 'name' => 'LOCAL VENDOR', 'short_description' => 'WBLocal offers to consumers access to widest assortment from local sellers', 'long_description' => '', 'details' => "['You are based within country', 'You are a registered business', 'Ship products to our local sort center', 'Access to wide array of seller tools']"]);
        }
        if (!VendorType::where('slug', 'global')->first()) {
            VendorType::firstOrCreate(['slug' => 'global', 'name' => 'W-BLUE GLOBAL', 'short_description' => 'WBGlobal offers to consumers access to widest assortment from local sellers', 'long_description' => '', 'details' => "['You are based from outside country', 'You are a registered business', 'Ship products to our oversea sort center', 'Access to wide array of seller tools']"]);
        }
    }
}
