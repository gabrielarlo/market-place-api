<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class SeedCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('data/categories.json');
        $json = file_get_contents($path);
        $categories = json_decode($json);

        foreach ($categories->categories as $category) {
            $parent = Category::firstOrCreate(['parent_id' => 0, 'name' => $category->name]);

            foreach ($category->sub_categories as $sub) {
                Category::firstOrCreate(['parent_id' => $parent->id, 'name' => $sub->name]);
            }
        }
    }
}
