<?php

use App\Models\State;
use App\Models\Shipping;
use App\Models\ShippingRate;
use Illuminate\Database\Seeder;

class SeedShippingRates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state_ids = State::where('state_id', 0)->where('status', 1)->pluck('id');
        $shippings = Shipping::where('status', 1)->get(['id', 'max_rate']);
        $ranges = [
            'w-express' => [6, 14],
            'economy' => [8, 15],
            'standard' => [6, 9],
            'express' => [3, 7],
        ];

        $ship_from_ids = $state_ids;
        $ship_to_ids = $state_ids;
        foreach ($shippings as $shipping) {
            foreach ($ship_from_ids as $ship_from_id) {
                foreach ($ship_to_ids as $ship_to_id) {
                    foreach ($ranges as $range) {
                        ShippingRate::firstOrCreate(['shipping_id' => $shipping->id, 'min_days' => $range[0], 'max_days' => $range[1], 'rate' => $shipping->max_rate, 'state_from_id' => $ship_from_id, 'state_to_id' => $ship_to_id]);
                    }
                }
            }
        }
    }
}
