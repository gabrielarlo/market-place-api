<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class SeedCountryList extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::firstOrCreate(['iso' => 'qat', 'name' => 'Qatar']);
        Country::firstOrCreate(['iso' => 'bhr', 'name' => 'Bahrain']);
        Country::firstOrCreate(['iso' => 'jor', 'name' => 'Jordan']);
        Country::firstOrCreate(['iso' => 'sau', 'name' => 'Saudi Arabia']);
        Country::firstOrCreate(['iso' => 'omn', 'name' => 'Oman']);
        Country::firstOrCreate(['iso' => 'are', 'name' => 'United Arab Emirates']);

        $country = Country::where('iso', 'qat')->first();
        $country->default = 1;
        $country->save();
    }
}
