<?php

use App\Models\Bank;
use Illuminate\Database\Seeder;

class SeedQatarBanks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('data/qat-banks.json');
        $json = file_get_contents($path);
        $banks = json_decode($json);

        $iso = 'qat';
        foreach ($banks->banks as $bank) {
            Bank::firstOrCreate(['name' => $bank->name, 'code' => $bank->code, 'iso' => $iso]);
        }
    }
}
