<?php

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SeedDefaultSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Settings::find('global')) {
            $data = [
                'commission' => [
                    'to' => 'company', // company|vendor
                    'mode' => 'percentage', //percentage|fixed
                    'value' => 20,
                    'after_consider_vendor_coupon' => true,
                    'after_consider_admin_coupon' => true,
                    'shipping_cost_goes_to_vendor' => true,
                    'tax_goes_to_vendor' => true,
                ],
                'withdrawal' => [
                    'auto_approve_request' => false,
                    'generate_auto_withdrawal' => false,
                    'allowed_order_status_for_withdrawal' => 'completed', //pending,processing,completed,on-delivery
                    'minimum_withdrawal_limit' => 10,
                    'days_before_can_be_withdraw' => 7,
                ]
            ];
            Settings::firstOrCreate(['scope' => 'global', 'data' => json_encode($data)]);
        }
    }
}
