<?php

use App\Models\ProductType;
use App\Models\StockStatus;
use Illuminate\Database\Seeder;

class SeedProductTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductType::firstOrCreate(['name' => 'simple']);
        ProductType::firstOrCreate(['name' => 'variable']);
        ProductType::firstOrCreate(['name' => 'group']);
        ProductType::firstOrCreate(['name' => 'digital']);
        ProductType::firstOrCreate(['name' => 'service']);

        StockStatus::firstOrCreate(['name' => 'in_stock']);
        StockStatus::firstOrCreate(['name' => 'out_of_stock']);
        StockStatus::firstOrCreate(['name' => 'on_backorder']);
    }
}
