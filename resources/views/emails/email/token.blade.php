@component('mail::message')
<h1 style="text-align: center;">Email Verification</h1>
<hr>

## Greetings {{ $user->name }},

<p style="text-align: center;">
    Thank you for following the instructions at {{ config('app.name') }}, here's your token below.
</p>

<div style="text-align: center; padding: 10px; border-radius: 3px; color: #000; background: #64b5f6; border: 1px dashed #111; font-family: monospace; font-size: larger;">
    {{ $user->email_token }}
</div>

<br>
Please do not reply, this is system generated email.
<br>
<br>
Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
