<?php

return [
    'not_found' => 'User Information not found',
    'not_allowed' => 'You are not allowed for this operation',
    'required_bdate' => 'Birthdate is required',
    'invalid_current_password' => 'Invalid current password',
    'same_mobile' => 'Unable to proceed due to same mobile number',
    'same_email' => 'Unable to proceed due to same email address',
    'existing_mobile' => 'Mobile Number is already used by other',
    'existing_email' => 'Email Address is already used by other',
];
