const server = require('http').createServer();

const io = require('socket.io')(server, {
    path: '/wblue',
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false
});

server.listen(3000, () =>
{
    console.log('Socket Server Started...');
});

io.on('connection', (socket) =>
{
    console.log('Connected:', socket.id);

    // LISTENER
    socket.on('terminal', (packet) =>
    {
        console.log('Terminal Packet: ', packet);
        packet = JSON.parse(packet);
        if (packet.sender_type == 'vendor' || packet.receiver_type == 'vendor') {
            if (packet.sender_id == packet.receiver_id) {
                return false;
            }
            packet.datetime = (new Date()).toISOString();
            broadcast(JSON.stringify(packet));
        }
    });

    socket.on('disconnect', (packet) =>
    {
        console.log('Disconnect Packet: ', packet);
    });
});

broadcast = ($data) =>
{
    // GLOBAL EMITTER
    io.emit('broadcast', $data);
};
